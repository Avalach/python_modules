###################################################################################################
#                                                                                                 #
#     Output module                                                                               #
#                                                                                                 #
###################################################################################################


#--------------------------------------------------------------------------------------------------
# Print output of an object:
# 
# Input variables:
# obj:             Object
#

def output_object(obj, **kwargs):

   if "comment_line" in kwargs and type(kwargs["comment_line"])==str:
      print(kwargs["comment_line"])
   print(obj)



#--------------------------------------------------------------------------------------------------
# Output a string as plain message:
#
# Input variables:
# string: A string printed on screen
#

def message_empty_line():

   print()



#--------------------------------------------------------------------------------------------------
# Output a string as plain message:
#
# Input variables:
# string: A string printed on screen
#

def message_line(string,**kwargs):

   if "comment_line" in kwargs and type(kwargs["comment_line"])==str:
      print(kwargs["comment_line"])
   if "headword" in kwargs and type(kwargs["headword"])==str:
      string = kwargs["headword"] + ": " + string
      if (kwargs["headword"] == "Fatal error"):
         message_empty_line()
         print(string)
         message_empty_line()
         message_box_array(["The program stops here!"], 60, align="center")
         if __name__ != "__main__":
            exit()
      else:
         print(string)
   else:
      print(string)



#--------------------------------------------------------------------------------------------------
# Output an array of strings as plain message:
#
# Input variables:
# string_array[idx_1] : One dimensional array of strings which is printed on screen
#

def message_line_array(string_array,**kwargs):

   if "comment_line" in kwargs and type(kwargs["comment_line"])==str:
      print(kwargs["comment_line"])
   string = ""
   if "headword" in kwargs:
      headword_string = kwargs["headword"]
      headword_length = len(headword_string)
      empty_string    = " " * headword_length
   for ii in range(0, len(string_array)):
      if "headword" in kwargs:
         if (ii == 0):
            print(headword_string + ": " + string_array[ii])
         else:
            print(empty_string + "  " + string_array[ii])
      else:
         print(string_array[ii])
   if ("headword" in kwargs and kwargs["headword"] == "Fatal error"):
      message_empty_line()
      message_box_array(["The program stops here!"], 60, align="center")
      if __name__ != "__main__":
         exit()



#--------------------------------------------------------------------------------------------------
# Output a string only of stars as message:
#
# Input variables:
# string_lenght: Length of the string
#

def message_character_line(string, string_length,**kwargs):

   if "comment_line" in kwargs and type(kwargs["comment_line"])==str:
      print(kwargs["comment_line"])
   string = string * string_length
   print(string)



#--------------------------------------------------------------------------------------------------
# Function to create an array of strings that are aligned like a table from:
#
# Input variables:
# string_array[idx_1,idx_2]: Two dimensional array of strings. First index is the table row number,
#                            the second index is the table element or column number
# max_string_length:         One dimensional array of integers that gives the string length for each column
#                            of the final string table
#
# Return variables:
# string_array[idx_1]:       One dimensional array of strings that express an aligned table
#

def align_string_array(string_array, max_string_length, **kwargs):

   err = False
   aligned_string_array = []
   line_number = 0
   hline_number = 0
   line_string_length = sum(max_string_length)
   for ii in range(0, len(string_array)):
      if "frame" in kwargs:
         string = kwargs["frame"] + " "
      else:
         string = ""
      if "hline" in kwargs:
         if hline_number < len(kwargs["hline"]):
            if (line_number == int(kwargs["hline"][hline_number])):
               hline_number += 1
               aligned_string_array.append("|" + "-" * line_string_length + "-" * (3 * len(max_string_length) - 1) + "|")
      line_number += 1
      for jj in range(0, len(max_string_length)):
         if "align" in kwargs:
            if (kwargs["align"] == "left"):
               if "frame" in kwargs:
                  string = string + ("{0:<"+str(max_string_length[jj])+"}").format(str(string_array[ii][jj])) + " " + kwargs["frame"] + " "
               else:
                  string = string + ("{0:<"+str(max_string_length[jj])+"}").format(str(string_array[ii][jj])) + "  "
            elif (kwargs["align"] == "right"):
               if "frame" in kwargs:
                  string = string + ("{0:>"+str(max_string_length[jj])+"}").format(str(string_array[ii][jj])) + " " + kwargs["frame"] + " "
               else:
                  string = string + ("{0:>"+str(max_string_length[jj])+"}").format(str(string_array[ii][jj])) + "  "
            elif (kwargs["align"] == "center"):
               if "frame" in kwargs:
                  string = string + ("{0:^"+str(max_string_length[jj])+"}").format(str(string_array[ii][jj])) + " " + kwargs["frame"] + " "
               else:
                  string = string + ("{0:^"+str(max_string_length[jj])+"}").format(str(string_array[ii][jj])) + "  "
            else:
               err = True
               break
         else:
            if "frame" in kwargs:
               string = string + ("{0:<"+str(max_string_length[jj])+"}").format(str(string_array[ii][jj])) + " " + kwargs["frame"] + " "
            else:
               string = string + ("{0:<"+str(max_string_length[jj])+"}").format(str(string_array[ii][jj])) + "  "
      if err:
         message_line("Wrong align statement in align option!",headword="Fatal error")
         break
      aligned_string_array.append(string)
   return aligned_string_array



#--------------------------------------------------------------------------------------------------
# Function to create a message box with an headliner and stars as boundaries
#
# Input variables:
#
# string_array[idx_1]:     One dimensional array of strings which should be printed on screen
# header_array[idx_1]:     One dimensional array of strings which should be printed as header
# box_width:               Integer number of the maximum charakter number for the message box size
#
# Return variables:
# box_line_array[idx_1]: One dimensional array of strings that express the box message
#

def message_box_array(string_array, box_width, **kwargs):

   box_line_array = []
   string = "*" * box_width
   box_line_array.append(string)

   if "comment_line" in kwargs and type(kwargs["comment_line"])==str:
      print(kwargs["comment_line"])

   if "header" in kwargs:
      for ii in range(0, len(kwargs["header"])):
         string = "* " + ("{0:^"+str(box_width-4)+"}").format(str(kwargs["header"][ii])) + " *"
         box_line_array.append(string)
      string = "*" * box_width
      box_line_array.append(string) 

   err = False
   for ii in range(0, len(string_array)):
      if "align" in kwargs:
         if (kwargs["align"] == "left"):
            string = "* " + ("{0:<"+str(box_width-4)+"}").format(str(string_array[ii])) + " *"
         elif (kwargs["align"] == "right"):
            string = "* " + ("{0:>"+str(box_width-4)+"}").format(str(string_array[ii])) + " *"
         elif (kwargs["align"] == "center"):
            string = "* " + ("{0:^"+str(box_width-4)+"}").format(str(string_array[ii])) + " *"
         else:
            err = True
            break
      else:
         string = "* " + ("{0:<"+str(box_width-4)+"}").format(str(string_array[ii])) + " *"
      box_line_array.append(string)
   if err:
      message_line("Wrong align statement in align option!",headword="Fatal error")
   else:
      string = "*" * box_width
      box_line_array.append(string)
      message_line_array(box_line_array)




###################################################################################################
#                       Testing the module classes and functions                                  #
###################################################################################################

if __name__ == "__main__":

   message_empty_line()
   message_box_array(["","Testing output functions",""], 100, align="center")
   message_empty_line()
   message_empty_line()

   message_empty_line()
   message_box_array(["Testing function: message_line(string)"], 100)
   message_empty_line()

   class Foo(object):
      pass

   my_object = Foo()
   my_object.foo = 'bar'

   message_line("Print object representation:")
   output_object(my_object)
   message_empty_line()
   message_line("Print object representation with comment line ahead:")
   output_object(my_object,comment_line="Out:")
   message_empty_line()
   message_empty_line()

   message_empty_line()
   message_box_array(["Testing function: message_line"], 100)
   message_empty_line()
   message_line("message_line(\"Test message of a string\",comment_line=\"Out:\")",comment_line="In:")
   message_empty_line()
   message_line("Test message of a string",comment_line="Out:")
   message_empty_line()
   message_empty_line()

   message_empty_line()
   message_box_array(["\"Testing function: message_line with headword \"Info\")\""], 100)
   message_empty_line()
   message_line("message_line(\"Test message of a string in info mode\"," \
                "headword=\"Info\",comment_line=\"Out:\")""",comment_line="In:")
   message_empty_line()
   message_line("Test message of a string in info mode",headword="Info",comment_line="Out:")
   message_empty_line()
   message_empty_line()

   message_empty_line()
   message_box_array(["Testing function: message_line with headword \"Warning\")"], 100)
   message_empty_line()
   message_line("message_line(\"Test message of a string in warning mode\","\
                "headword=\"Warning\",comment_line=\"Out:\")",comment_line="In:")
   message_empty_line()
   message_line("Test message of a string in warning mode",headword="Warning",comment_line="Out:")
   message_empty_line()
   message_empty_line()

   message_empty_line()
   message_box_array(["Testing function: message_line with headword \"Error\")"], 100)
   message_empty_line()
   message_line("message_line(\"Test message of a string in error mode\"," \
                "headword=\"Error\",comment_line=\"Out:\")",comment_line="In:")
   message_empty_line()
   message_line("Test message of a string in error mode",headword="Error",comment_line="Out:")
   message_empty_line()
   message_empty_line()

   message_empty_line()
   message_box_array(["Testing function: message_line with headword \"Fatal error\")"], 100)
   message_empty_line()
   message_line("message_line(\"Test message of a string in fatal error mode\"," \
                "headword=\"Fatal error\",comment_line=\"Out:\")",comment_line="In:")
   message_empty_line()
   message_line("Test message of a string in fatal error mode",headword="Fatal error",comment_line="Out:")
   message_empty_line()
   message_empty_line()

   message_empty_line()
   message_box_array(["Testing function: message_line_array"], 100)
   message_empty_line()
   message_line("message_line_array([\"Test array element 1\", \"Test array element 2\"]," \
                "comment_line=\"Out:\")",comment_line="In:")
   message_empty_line()
   message_line_array(["Test array element 1", "Test array element 2"],comment_line="Out:")
   message_empty_line()
   message_empty_line()

   message_empty_line()
   message_box_array(["Testing function: message_line_array with headword \"Info\""], 100)
   message_empty_line()
   message_line("message_line_array([\"Test array element 1\", \"Test array element 2\"]," \
                "headword=\"Info\",comment_line=\"Out:\")",comment_line="In:")
   message_empty_line()
   message_line_array(["Test array element 1", "Test array element 2"],headword="Info",comment_line="Out:")
   message_empty_line()
   message_empty_line()

   message_empty_line()
   message_box_array(["Testing function: message_warning_line_array with headword \"Warning\""], 100)
   message_empty_line()
   message_line("message_line_array([\"Test array element 1\", \"Test array element 2\"]," \
                "headword=\"Warning\",comment_line=\"Out:\")",comment_line="In:")
   message_empty_line()
   message_line_array(["Test array element 1", "Test array element 2"],headword="Warning",comment_line="Out:")
   message_empty_line()
   message_empty_line()

   message_empty_line()
   message_box_array(["Testing function: message_line_array with headword \"Error\""], 100)
   message_empty_line()
   message_line("message_line_array([\"Test array element 1\", \"Test array element 2\"]," \
                "headword=\"Error\",comment_line=\"Out:\")",comment_line="In:")
   message_empty_line()
   message_line_array(["Test array element 1", "Test array element 2"],headword="Error",comment_line="Out:")
   message_empty_line()
   message_empty_line()

   message_empty_line()
   message_box_array(["Testing function: message_line_array with headword \"Fatal error\""], 100)
   message_empty_line()
   message_line("message_line_array([\"Test array element 1\", \"Test array element 2\"],"
                "headword=\"Fatal error\",comment_line=\"Out:\")",comment_line="In:")
   message_empty_line()
   message_line_array(["Test array element 1", "Test array element 2"],headword="Fatal error",comment_line="Out")
   message_empty_line()
   message_empty_line()

   message_empty_line()
   message_box_array(["Testing function: message_character_line"], 100)
   message_empty_line()
   message_line("message_character_line(\"*\",50)")
   message_empty_line()
   message_character_line("*",50)
   message_empty_line()
   message_empty_line()

   message_empty_line()
   message_box_array(["Testing function: align_string_array(string_array, max_string_length, **kwargs)"], 100)
   message_empty_line()
   message_line("Left aligned table:")
   message_empty_line()
   message_line("string_array = align_string_array([[\"column name 1\", \"column name 2\"],[\"value 1.1\",\"value 1.2a\"]," \
                "[\"value 2.1\", \"value 2.2\"]], [20,20])",comment_line="In:")
   message_empty_line()
   string_array = align_string_array([["column name 1", "column name 2"],["value 1.1","value 1.2"], \
                                     ["value 2.1", "value 2.2"]], [20,20])
   message_line_array(string_array)
   message_empty_line()
   message_character_line("*",70)
   message_empty_line()
   message_line("Centered aligned table:")
   message_empty_line()
   message_line("string_array = align_string_array([[\"column name 1\", \"column name 2\"],[\"value 1.1\",\"value 1.2\"]," \
                "[\"value 2.1\", \"value 2.2\"]], [20,20], align=\"center\")",comment_line="In:")
   string_array = align_string_array([["column name 1", "column name 2"],["value 1.1","value 1.2"], \
                                     ["value 2.1", "value 2.2"]], [20,20], align="center")
   message_line_array(string_array)
   message_empty_line()
   message_character_line("*",70)
   message_empty_line()
   message_line("Right aligned table:")
   message_empty_line()
   message_line("string_array = align_string_array([[\"column name 1\", \"column name 2\"],[\"value 1.1\",\"value 1.2\"]," \
                "[\"value 2.1\", \"value 2.2\"]], [20,20], align=\"right\")",comment_line="In:")
   message_empty_line()
   string_array = align_string_array([["column name 1", "column name 2"],["value 1.1","value 1.2"], \
                                     ["value 2.1", "value 2.2"]], [20,20], align="right")
   message_line_array(string_array)
   message_empty_line()
   message_character_line("*",70)
   message_box_array(["Testing function: align_string_array(string_array, max_string_length, **kwargs)"], 100)
   message_empty_line()
   message_line("Left aligned table:")
   message_empty_line()
   message_line("string_array = align_string_array([[\"column name 1\", \"column name 2\"],[\"value 1.1\",\"value 1.2\"]," \
                "[\"value 2.1\", \"value 2.2\"]], [20,20])",comment_line="In:")
   message_empty_line()
   string_array = align_string_array([["column name 1", "column name 2"],["value 1.1","value 1.2"], \
                                     ["value 2.1", "value 2.2"]], [20,20], frame="|", hline=[1])
   message_line_array(string_array)
   message_empty_line()
   message_character_line("*",70)
   message_empty_line()
   message_line("Centered aligned table:")
   message_empty_line()
   message_line("string_array = align_string_array([[\"column name 1\", \"column name 2\"],[\"value 1.1\",\"value 1.2\"]," \
                "[\"value 2.1\", \"value 2.2\"]], [20,20], align=\"center\")",comment_line="In:")
   string_array = align_string_array([["column name 1", "column name 2"],["value 1.1","value 1.2"], \
                                     ["value 2.1", "value 2.2"]], [20,20], align="center", frame="|", hline=[1])
   message_line_array(string_array)
   message_empty_line()
   message_character_line("*",70)
   message_empty_line()
   message_line("Right aligned table:")
   message_empty_line()
   message_line("string_array = align_string_array([[\"column name 1\", \"column name 2\"],[\"value 1.1\",\"value 1.2\"]," \
                "[\"value 2.1\", \"value 2.2\"]], [20,20], align=\"right\")",comment_line="In:")
   message_empty_line()
   string_array = align_string_array([["column name 1", "column name 2"],["value 1.1","value 1.2"], \
                                     ["value 2.1", "value 2.2"]], [20,20], align="right", frame="|", hline=[1])
   message_line_array(string_array)
   message_empty_line()
   message_character_line("*",70)
   message_empty_line()
   message_line("Error message for wrong align statement:")
   message_empty_line()
   message_line("string_array = align_string_array([[\"column name 1\", \"column name 2\"],[\"value 1.1\",\"value 1.2\"]," \
                "[\"value 2.1\", \"value 2.2\"]], [20,20], align=\"error\")",comment_line="In:")
   message_empty_line()
   string_array = align_string_array([["column name 1", "column name 2"],["value 1.1","value 1.2"], \
                                     ["value 2.1", "value 2.2"]], [20,20], align="error")
   message_line_array(string_array)
   message_empty_line()
   message_empty_line()

   message_empty_line()
   message_box_array(["Testing function: message_box_array(string_array, box_width, **kwargs)"], 100)
   message_empty_line()
   message_line("Left aligned message box without an header:")
   message_empty_line()
   message_line("message_box_array([\"Test message for message box in line 1\",\"Test message for message box in line 2\"]," \
                "60, comment_line=\"Out:\")",comment_line="In:")
   message_empty_line()
   message_box_array(["Test message for message box in line 1","Test message for message box in line 2"], 60, comment_line="Out:")
   message_empty_line()
   message_character_line("*",70)
   message_empty_line()
   message_line("Left aligned message box wit an header:")
   message_empty_line()
   message_line("message_box_array([\"Test message for message box in line 1\",\"Test message for message box in line 2\"]," \
                "60, header=[\"header line 1\",\"header line 2\"],comment_line=\"Out:\")",comment_line="In:")
   message_empty_line()
   message_box_array(["Test message for message box in line 1","Test message for message box in line 2"], \
                     60, header=["header line 1","header line 2"],comment_line="Out:")
   message_empty_line()
   message_character_line("*",70)
   message_empty_line()
   message_line("Center aligned message box wit an header:")
   message_empty_line()
   message_line("message_box_array([\"Test message for message box in line 1\",\"Test message for message box in line 2\"]," \
                "60, header=[\"header line 1\",\"header line 2\"], align=\"center\",comment_line=\"Out:\")",comment_line="In:")
   message_empty_line()
   message_box_array(["Test message for message box in line 1","Test message for message box in line 2"], \
                     60, header=["header line 1","header line 2"], align="center",comment_line="Out:")
   message_empty_line()
   message_character_line("*",70)
   message_empty_line()
   message_line("Center aligned message box wit an header:")
   message_empty_line()
   message_line("message_box_array([\"Test message for message box in line 1\",\"Test message for message box in line 2\"]," \
                "60, header=[\"header line 1\",\"header line 2\"], align=\"right\",comment_line=\"Out:\")",comment_line="In:")
   message_empty_line()
   message_box_array(["Test message for message box in line 1","Test message for message box in line 2"], \
                     60, header=["header line 1","header line 2"], align="right",comment_line="Out:")
   message_empty_line()
   message_character_line("*",70)
   message_empty_line()
   message_line("Error message for wrong align statement:")
   message_empty_line()
   message_line("message_box_array([\"Test message for message box in line 1\",\"Test message for message box in line 2\"]," \
                "60, header=[\"header line 1\",\"header line 2\"], align=\"error\",comment_line=\"Out:\")",comment_line="In:")
   message_empty_line()
   message_box_array(["Test message for message box in line 1","Test message for message box in line 2"], \
                     60, header=["header line 1","header line 2"], align="error",comment_line="Out:")
   message_empty_line()
   message_empty_line()


   

