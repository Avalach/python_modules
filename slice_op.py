###################################################################################################
#                                                                                                 #
#     Slice operations                                                                            #
#                                                                                                 #
###################################################################################################


def str_to_int_idx(str_list):
   idx_list = []
   for ii in range(0,len(str_list)):
         idx_list.append(ii+1)
   return idx_list


def str_slice(str_list, *args): 
   if (args):
      start_idx = str_list.index(args[0]) + 1
      if (len(args) > 1):
         end_idx = str_list.index(args[1]) + 1
      else:
         end_idx = start_idx
   else:
      start_idx = 1
      end_idx = len(str_list)
   return slice(start_idx, end_idx, 1)


def int_slice(int_list, *args):
   if (args):
      start_idx = args[0]
      if (len(args) > 1):
         end_idx = args[1]
      else:
         end_idx = start_idx
   else:
      start_idx = 1
      end_idx = len(int_list)
   return slice(start_idx, end_idx, 1)


#class string_slice:
#   def index_list(self, full_list, start_str, *end_str):
#      idx_list = []
#      start_idx = full_list.index(start_str)
#      if (not end_str):
#         idx_list.append(start_idx)
#         return idx_list
#      else:
#         end_idx = full_list.index(end_str)
#         for ii in range(start_idx,end_idx+1):
#            idx_list.append(ii)
#         return idx_list


if __name__ == "__main__":

   slc = string_slice()
   print(slc.index_list(["this", "is", "a", "test", "list"], "is", "test"))

   

