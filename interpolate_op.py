###################################################################################################
#                                                                                                 #
#     Interpolation operations                                                                    #
#                                                                                                 #
###################################################################################################

import numpy             as np
import matplotlib.pyplot as plt
import plot_op           as plo
import output            as outp
import math_op           as mop
import grid_op           as gro

from scipy   import interpolate


def interpolation(grid_points, grid_values, interpolation_points, interpolation_type, dim, interpolation_method):
   if interpolation_type == 'interp':
      if (dim == 1):
         interpolation_function = interpolate.interp1d(grid_points, grid_values, kind=interpolation_method)
         return interpolation_function(interpolation_points)
      if (dim == 2):
         grid_points_mesh = gro.meshgrid(grid_points[0], grid_points[1])
         grid_values_mesh = []
         idx = 0
         for ii in range(0, len(grid_points[0])):
            aa = []
            for jj in range(0, len(grid_points[1])):
               aa.append(grid_values[idx])
               idx += 1
            grid_values_mesh.append(aa)
         interpolation_function = interpolate.interp2d(grid_points_mesh[0], grid_points_mesh[1], grid_values_mesh, kind=interpolation_method)
         return interpolation_function(interpolation_points[0], interpolation_points[1])
   if interpolation_type == 'griddata':
      if (dim == 1):
         return interpolate.griddata(grid_points, grid_values, interpolation_points, method=interpolation_method) 
      if (dim == 2):
         interpolation_points_mesh = [None]*2
         interpolation_points_mesh[0], interpolation_points_mesh[1] = gro.inverse_meshgrid(interpolation_points[0],interpolation_points[1])
         return interpolate.griddata(grid_points, grid_values, (interpolation_points_mesh[0], interpolation_points_mesh[1]), method=interpolation_method)


def minmax(xx):
   xx_min     = np.amin(xx)
   xx_min_idx = np.where(xx == np.amin(xx))[0][0]
   xx_max     = np.amax(xx)
   xx_max_idx = np.where(xx == np.amax(xx))[0][0]
   return xx_min, xx_min_idx, xx_max, xx_max_idx


def nearest_value(xx, xx_0):
   abs_diff_func = lambda xx_value : abs(xx_value - xx_0)
   xx_closest     = min(xx, key = abs_diff_func)
   xx_closest_idx = np.where(xx == xx_closest)[0][0]
   return xx_closest, xx_closest_idx


def measure_segment(xx, yy, yy_0, delta_yy):
   yy_1, yy_1_idx = nearest_value(yy, yy_0)
   yy_2 = yy_1 + delta_yy
   segment_xx = []
   segment_yy = []
   aa = []
   bb = []
   count = 0
   for ii in range(0, len(xx)):
      if ((yy[ii] >= yy_1) and (yy[ii] <= yy_2)):
         if len(aa) == 0:
            aa.append(xx[ii])
            bb.append(yy[ii])       
         elif (aa[len(aa)-1] == xx[ii-1]):
            aa.append(xx[ii])
            bb.append(yy[ii])
         else:
            segment_xx.append(aa)
            segment_yy.append(bb)
            aa = []
            bb = []
            aa.append(xx[ii])
            bb.append(yy[ii])
   segment_xx.append(aa)
   segment_yy.append(bb)
   return segment_xx, segment_yy


def measure_segments(xx, yy, number_segments):
   yy_min, yy_min_idx, yy_max, yy_max_idx = minmax(yy)
   delta_yy = (yy_max-yy_min)/number_segments
   segment_xx_list = []
   segment_yy_list = []
   for ii in range(0, number_segments):
      aa, bb = measure_segment(xx, yy, yy_min+ii*delta_yy, delta_yy)
      segment_xx_list.append(aa)
      segment_yy_list.append(bb)
   return segment_xx_list, segment_yy_list


def convolution(xx_ff, xx_gg, ff, gg, **kwargs):
   delta_xx_ff = xx_ff[1] - xx_ff[0]
   delta_xx_gg = xx_gg[1] - xx_gg[0]
   if (not (delta_xx_ff == delta_xx_gg)):
      outp.message_line("The two x-axis spacings have to be equivalent",headword="Fatal error")
   xx_return = []
   xx_min = min(xx_ff)-max(xx_gg)
   xx_max = max(xx_ff)+max(xx_gg)
   integral_norm_factor = (xx_max-xx_min) / (len(xx_ff)+len(xx_gg))
   print(integral_norm_factor)
   if kwargs['mode'] == 'full':
      for ii in range(0, len(xx_ff)+len(xx_gg)-1):
         xx_return.append(xx_min + ii * delta_xx_ff)
   elif kwargs['mode'] == 'same':
      if (len(xx_ff) >= len(xx_gg)):
         xx_return = xx_ff
      else:
         xx_return = xx_gg
   conv_return = integral_norm_factor * np.convolve(ff, gg, **kwargs) 
   return xx_return, conv_return





###################################################################################################
#                       Testing the module classes and functions                                  #
###################################################################################################

if __name__ == "__main__":

   interpolation_1D = False
   interpolation_2D = False
   measure_segs     = False
   convolution_test = True



   plot_points = []
   plot_values = []

   grid_points = []
   grid_values = []

   plot_points.append(np.linspace(0, 2*np.pi, 200))
   plot_values.append(np.sin(plot_points[0]))

   plot_points.append(np.linspace(0, 2*np.pi, 200))
   plot_values.append(np.sin(2*plot_points[1]))

   plot_points.append(np.linspace(0, 2*np.pi, 200))
   plot_values.append(np.cos(2*plot_points[1]))


   gp_number = 7
   grid_points.append(np.linspace(0, 2*np.pi, gp_number))
   grid_points.append(np.linspace(0, 2*np.pi, gp_number))

   grid_values.append(np.sin(grid_points[0]))
   grid_values.append(np.cos(grid_points[0]))
   grid_values.append(np.exp(grid_points[0]))

   grid_points_2D = gro.combination_vector([grid_points[0],grid_points[1]])

   grid_values_2D = []
   [ grid_values_2D.append(np.sin(ii) * np.sin(2*jj)) for ii in grid_points[0] for jj in grid_points[1] ]

   plot_values_2D = []
   [ plot_values_2D.append(np.sin(ii) * np.sin(2*jj)) for ii in plot_points[0] for jj in plot_points[1] ]



   if interpolation_1D:

      interp_zero      = interpolation(grid_points[0], grid_values[0], plot_points[0], 'interp', 1, 'zero')
      interp_nearest   = interpolation(grid_points[0], grid_values[0], plot_points[0], 'interp', 1, 'nearest')
      interp_next      = interpolation(grid_points[0], grid_values[0], plot_points[0], 'interp', 1, 'next')
      interp_previous  = interpolation(grid_points[0], grid_values[0], plot_points[0], 'interp', 1, 'previous')
      interp_linear    = interpolation(grid_points[0], grid_values[0], plot_points[0], 'interp', 1, 'linear')
      interp_slinear   = interpolation(grid_points[0], grid_values[0], plot_points[0], 'interp', 1, 'slinear')
      interp_quadratic = interpolation(grid_points[0], grid_values[0], plot_points[0], 'interp', 1, 'quadratic')
      interp_cubic     = interpolation(grid_points[0], grid_values[0], plot_points[0], 'interp', 1, 'cubic')

      griddata_nearest = interpolation(grid_points[0], grid_values[0], plot_points[0], 'griddata', 1, 'nearest')
      griddata_linear  = interpolation(grid_points[0], grid_values[0], plot_points[0], 'griddata', 1, 'linear')
      griddata_cubic   = interpolation(grid_points[0], grid_values[0], plot_points[0], 'griddata', 1, 'cubic')


      plt.figure(1,figsize=(6.4,7.5))
      plt.ylim(0.0, 2*np.pi)
      plt.ylim(-1.2,1.2)

      plt.subplot(3,2,1)
      plt.plot(plot_points[0], plot_values[0], 'b')

      plt.subplot(3,2,3)
      plt.plot(plot_points[0], plot_values[0], 'b')
      plt.plot(plot_points[0], interp_zero, 'r')

      plt.subplot(3,2,4)
      plt.plot(plot_points[0], plot_values[0], 'b')
      plt.plot(plot_points[0], interp_nearest, 'r')

      plt.subplot(3,2,5)
      plt.plot(plot_points[0], plot_values[0], 'b')
      plt.plot(plot_points[0], interp_next, 'r')

      plt.subplot(3,2,6)
      plt.plot(plot_points[0], plot_values[0], 'b')
      plt.plot(plot_points[0], interp_previous, 'r')

      plt.subplots_adjust(hspace=0.5)


      plt.figure(2,figsize=(6.4,7.5))
      plt.ylim(0.0, 2*np.pi)
      plt.ylim(-1.2,1.2)

      plt.subplot(3,2,1)
      plt.plot(plot_points[0], plot_values[0], 'b')

      plt.subplot(3,2,3)
      plt.plot(plot_points[0], plot_values[0], 'b')
      plt.plot(plot_points[0], interp_linear, 'r')

      plt.subplot(3,2,4)
      plt.plot(plot_points[0], plot_values[0], 'b')
      plt.plot(plot_points[0], interp_slinear, 'r')

      plt.subplot(3,2,5)
      plt.plot(plot_points[0], plot_values[0], 'b')
      plt.plot(plot_points[0], interp_quadratic, 'r')

      plt.subplot(3,2,6)
      plt.plot(plot_points[0], plot_values[0], 'b')
      plt.plot(plot_points[0], interp_cubic, 'r')

      plt.subplots_adjust(hspace=0.5)


      plt.figure(3)
      plt.ylim(0.0, 2*np.pi)
      plt.ylim(-1.2,1.2)

      plt.subplot(2,2,1)
      plt.plot(plot_points[0], plot_values[0], 'b')

      plt.subplot(2,2,2)
      plt.plot(plot_points[0], plot_values[0], 'b')
      plt.plot(plot_points[0], griddata_nearest, 'r')

      plt.subplot(2,2,3)
      plt.plot(plot_points[0], plot_values[0], 'b')
      plt.plot(plot_points[0], griddata_linear, 'r')

      plt.subplot(2,2,4)
      plt.plot(plot_points[0], plot_values[0], 'b')
      plt.plot(plot_points[0], griddata_cubic, 'r')

      plt.subplots_adjust(hspace=0.5)



   if interpolation_2D:

      interp_linear    = interpolation(grid_points, grid_values_2D, plot_points, 'interp', 2, 'linear')
      interp_cubic     = interpolation(grid_points, grid_values_2D, plot_points, 'interp', 2, 'cubic')
      interp_quintic   = interpolation(grid_points, grid_values_2D, plot_points, 'interp', 2, 'quintic')

      griddata_nearest = interpolation(grid_points_2D, grid_values_2D, plot_points, 'griddata', 2, 'nearest')
      griddata_linear  = interpolation(grid_points_2D, grid_values_2D, plot_points, 'griddata', 2, 'linear')
      griddata_cubic   = interpolation(grid_points_2D, grid_values_2D, plot_points, 'griddata', 2, 'cubic')

      plt.figure(4,figsize=(10,10))
      plt.ylim(0.0, 2*np.pi)
      plt.ylim(-1.2,1.2)

      plt.subplot(2,2,1)
      cplt_lines, cplt_fill = plo.plot_contour_lines_and_fill(plot_points[0], plot_points[1], plot_values_2D, cf={'cmap':"RdBu_r"}, cl={'cmap':"RdBu_r"})
      plt.colorbar(cplt_fill)

      plt.subplot(2,2,2)
      cplt_lines, cplt_fill = plo.plot_contour_lines_and_fill(plot_points[0], plot_points[1], interp_linear, cf={'cmap':"RdBu_r"}, cl={'cmap':"RdBu_r"})
      plt.colorbar(cplt_fill)

      plt.subplot(2,2,3)
      cplt_lines, cplt_fill = plo.plot_contour_lines_and_fill(plot_points[0], plot_points[1], interp_cubic, cf={'cmap':"RdBu_r"}, cl={'cmap':"RdBu_r"})
      plt.colorbar(cplt_fill)

      plt.subplot(2,2,4)
      cplt_lines, cplt_fill = plo.plot_contour_lines_and_fill(plot_points[0], plot_points[1], interp_quintic, cf={'cmap':"RdBu_r"}, cl={'cmap':"RdBu_r"})
      plt.colorbar(cplt_fill)

      plt.subplots_adjust(hspace=0.5)


      plt.figure(5,figsize=(10,10))
      plt.ylim(0.0, 2*np.pi)
      plt.ylim(-1.2,1.2)

      plt.subplot(2,2,1)
      cplt_lines, cplt_fill = plo.plot_contour_lines_and_fill(plot_points[0], plot_points[1], plot_values_2D, cf={'cmap':"RdBu_r"}, cl={'cmap':"RdBu_r"})
      plt.colorbar(cplt_fill)

      plt.subplot(2,2,2)
      cplt_lines, cplt_fill = plo.plot_contour_lines_and_fill(plot_points[0], plot_points[1], griddata_nearest, cf={'cmap':"RdBu_r"}, cl={'cmap':"RdBu_r"})
      plt.colorbar(cplt_fill)

      plt.subplot(2,2,3)
      cplt_lines, cplt_fill = plo.plot_contour_lines_and_fill(plot_points[0], plot_points[1], griddata_linear, cf={'cmap':"RdBu_r"}, cl={'cmap':"RdBu_r"})
      plt.colorbar(cplt_fill)

      plt.subplot(2,2,4)
      cplt_lines, cplt_fill = plo.plot_contour_lines_and_fill(plot_points[0], plot_points[1], griddata_cubic, cf={'cmap':"RdBu_r"}, cl={'cmap':"RdBu_r"})
      plt.colorbar(cplt_fill)



   if measure_segs:

      yy_min, yy_min_idx, yy_max, yy_max_idx = minmax(plot_values[0])

      xx_segments_list, yy_segments_list = measure_segments(plot_points[0], plot_values[2], 5)
      seg_1_1_x = xx_segments_list[0][0]
      seg_1_1_y = yy_segments_list[0][0]
      seg_1_2_x = xx_segments_list[0][1]
      seg_1_2_y = yy_segments_list[0][1] 

      seg_2_1_x = xx_segments_list[1][0]
      seg_2_1_y = yy_segments_list[1][0]
      seg_2_2_x = xx_segments_list[1][1]
      seg_2_2_y = yy_segments_list[1][1]
      seg_2_3_x = xx_segments_list[1][2]
      seg_2_3_y = yy_segments_list[1][2]
      seg_2_4_x = xx_segments_list[1][3]
      seg_2_4_y = yy_segments_list[1][3]
 
      seg_3_1_x = xx_segments_list[2][0]
      seg_3_1_y = yy_segments_list[2][0]
      seg_3_2_x = xx_segments_list[2][1]
      seg_3_2_y = yy_segments_list[2][1]
      seg_3_3_x = xx_segments_list[2][2]
      seg_3_3_y = yy_segments_list[2][2]
      seg_3_4_x = xx_segments_list[2][3]
      seg_3_4_y = yy_segments_list[2][3]

      seg_4_1_x = xx_segments_list[3][0]
      seg_4_1_y = yy_segments_list[3][0]
      seg_4_2_x = xx_segments_list[3][1]
      seg_4_2_y = yy_segments_list[3][1]
      seg_4_3_x = xx_segments_list[3][2]
      seg_4_3_y = yy_segments_list[3][2]
      seg_4_4_x = xx_segments_list[3][3]
      seg_4_4_y = yy_segments_list[3][3] 

      seg_5_1_x = xx_segments_list[4][0]
      seg_5_1_y = yy_segments_list[4][0]
      seg_5_2_x = xx_segments_list[4][1]
      seg_5_2_y = yy_segments_list[4][1]
      seg_5_3_x = xx_segments_list[4][2]
      seg_5_3_y = yy_segments_list[4][2]

      ax1 = plt.figure(6,figsize=(10,10))
      plt.plot(plot_points[0], plot_values[2], 'k')
      plt.fill_between(seg_1_1_x, min(seg_1_1_y), max(seg_1_1_y), color='b', alpha=0.5)
      plt.fill_between(seg_1_2_x, min(seg_1_2_y), max(seg_1_2_y), color='b', alpha=0.5)
      plt.fill_between(seg_2_1_x, min(seg_2_1_y), max(seg_2_1_y), color='r', alpha=0.5)
      plt.fill_between(seg_2_2_x, min(seg_2_2_y), max(seg_2_2_y), color='r', alpha=0.5)
      plt.fill_between(seg_2_3_x, min(seg_2_3_y), max(seg_2_3_y), color='r', alpha=0.5)
      plt.fill_between(seg_2_4_x, min(seg_2_4_y), max(seg_2_4_y), color='r', alpha=0.5)
      plt.fill_between(seg_3_1_x, min(seg_3_1_y), max(seg_3_1_y), color='g', alpha=0.5)
      plt.fill_between(seg_3_2_x, min(seg_3_2_y), max(seg_3_2_y), color='g', alpha=0.5)
      plt.fill_between(seg_3_3_x, min(seg_3_3_y), max(seg_3_3_y), color='g', alpha=0.5)
      plt.fill_between(seg_3_4_x, min(seg_3_4_y), max(seg_3_4_y), color='g', alpha=0.5)
      plt.fill_between(seg_4_1_x, min(seg_4_1_y), max(seg_4_1_y), color='y', alpha=0.5)
      plt.fill_between(seg_4_2_x, min(seg_4_2_y), max(seg_4_2_y), color='y', alpha=0.5)
      plt.fill_between(seg_4_3_x, min(seg_4_3_y), max(seg_4_3_y), color='y', alpha=0.5)
      plt.fill_between(seg_4_4_x, min(seg_4_4_y), max(seg_4_4_y), color='y', alpha=0.5)
      plt.fill_between(seg_5_1_x, min(seg_5_1_y), max(seg_5_1_y), color='c', alpha=0.5)
      plt.fill_between(seg_5_2_x, min(seg_5_2_y), max(seg_5_2_y), color='c', alpha=0.5)
      plt.fill_between(seg_5_3_x, min(seg_5_3_y), max(seg_5_3_y), color='c', alpha=0.5)



   if convolution_test:

      xx_ff = [-0.5, -0.25, 0.0, 0.25, 0.5]
      ff    = [1.0, 1.0, 1.0, 1.0, 1.0]
      xx_gg = [-0.5, -0.25, 0.0, 0.25, 0.5]
      gg    = [1.0, 1.0, 1.0, 1.0, 1.0]

      ax1 = plt.figure(7,figsize=(10,10))

      xx_conv, ff_conv = convolution(xx_ff, xx_gg, ff, gg, mode='full')
      plt.plot(xx_conv, ff_conv)
 
      xx_conv, ff_conv = convolution(xx_ff, xx_gg, ff, gg, mode='same')
      plt.plot(xx_conv, ff_conv)




   plt.show()


