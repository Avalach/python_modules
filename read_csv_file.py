###################################################################################################
#
#     Module for reading csv-files with csv module
#
###################################################################################################


import csv
import pandas as pd

from collections     import defaultdict
from python_ml_files import output      as outp


#--------------------------------------------------------------------------------------------------
# Return first row of a csv file:
def read_csv_file_first_row(file_name, delimiter_inp):
   with open(file_name) as csv_file:
      csv_reader = csv.reader(csv_file, delimiter=delimiter_inp)
      line_count = 0
      for row in csv_reader:
         line_count += 1
         if (line_count == 1):
            return row

#--------------------------------------------------------------------------------------------------
# Return all rows of a csv file:
def read_csv_file_all_rows(file_name, delimiter_inp):
   with open(file_name) as csv_file:
      csv_reader = csv.reader(csv_file, delimiter=delimiter_inp)
      line_count = 0
      for row in csv_reader:
         line_count += 1
         return row

#--------------------------------------------------------------------------------------------------
# Return the number of rows of a csv file:
def read_csv_file_row_number(file_name, delimiter_inp):
   with open(file_name) as csv_file:
      csv_reader = csv.reader(csv_file, delimiter=delimiter_inp)
      line_count = 0
      for row in csv_reader:
         #print(row)
         line_count += 1
      return line_count-1

#--------------------------------------------------------------------------------------------------
# Return the number of columns of a csv file:
def read_csv_file_column_number(file_name, delimiter_inp):
   with open(file_name) as csv_file:
      csv_reader = csv.reader(csv_file, delimiter=delimiter_inp)
      line_count = 0
      for row in csv_reader:
         line_count += 1
         if (line_count == 1):
            return len(row)

#--------------------------------------------------------------------------------------------------
# Making an array of a specific csv file columns:
def read_csv_file_array(file_name, delimiter_inp, invalid_csv_str, invalid_element_str, *column_types):
   csv_column_names = read_csv_file_first_row(file_name, delimiter_inp)
   csv_column_number = read_csv_file_column_number(file_name, delimiter_inp)
   columns = defaultdict(list)
   columns_max_str = defaultdict(list)
   for kk in range(0, csv_column_number):
      columns_max_str[csv_column_names[kk]] = 0
   with open(file_name) as csv_file:
      csv_reader = csv.DictReader(csv_file, delimiter=delimiter_inp)
      if (column_types):
         for row in csv_reader:
            for (ii,jj) in row.items():
               max_str = columns_max_str[ii]
               if (max_str < len(jj) or max_str < len(invalid_element_str) or max_str < len(ii)):
                  columns_max_str[ii] = max(len(jj),len(invalid_element_str),len(ii))
               for kk in range(0, csv_column_number):
                  if (csv_column_names[kk] == ii ):
                     column_type = column_types[0][kk]
               if (jj == invalid_csv_str):
                  columns[ii].append(invalid_element_str)
               else:
                  if (column_type == "string"):
                     columns[ii].append(str(jj))
                  elif (column_type == "boolean"):
                     columns[ii].append(bool(int(jj)))
                  elif (column_type == "integer"):
                     columns[ii].append(int(jj))
                  elif (column_type == "float"):
                     columns[ii].append(float(jj))
                  else:
                     columns[ii].append(str(jj))
      else:
         for row in csv_reader:
            for (ii,jj) in row.items():
               max_str = columns_max_str[ii]
               if (max_str < len(jj) or max_str < len(invalid_element_str) or max_str < len(ii)):
                  columns_max_str[ii] = max(len(jj),len(invalid_element_str),len(ii))
               if (jj == invalid_csv_str):
                  columns[ii].append(invalid_element_str)
               else:
                  columns[ii].append(jj)
   return columns

#--------------------------------------------------------------------------------------------------
# Making an array of a specific csv file columns:
def read_csv_file_array_max_str(file_name, delimiter_inp, invalid_csv_str, invalid_element_str):
   csv_column_names = read_csv_file_first_row(file_name, delimiter_inp)
   csv_column_number = read_csv_file_column_number(file_name, delimiter_inp)
   columns = defaultdict(list)
   columns_max_str = defaultdict(list)
   for kk in range(0, csv_column_number):
      columns_max_str[csv_column_names[kk]] = 0
   with open(file_name) as csv_file:
      csv_reader = csv.DictReader(csv_file, delimiter=delimiter_inp)
      for row in csv_reader:
         for (ii,jj) in row.items():
            max_str = columns_max_str[ii]
            if (max_str < len(jj) or max_str < len(invalid_element_str) or max_str < len(ii)):
               columns_max_str[ii] = max(len(jj),len(invalid_element_str),len(ii))
   return columns_max_str





###################################################################################################
#
#     Reading csv-files with csv module and create datafram classes
#
###################################################################################################

#--------------------------------------------------------------------------------------------------
# Return first row of a csv file:
class csv_dataframe:

   def __init__(self, file_name, delimiter_inp, invalid_csv_str, invalid_element_str, *column_types):
      self.file_name                           = file_name
      self.delimiter                           = delimiter_inp
      self.invalid_csv_str                     = invalid_csv_str
      self.invalid_element_str                 = invalid_element_str
      self.row_number                          = read_csv_file_row_number(file_name, delimiter_inp)
      self.column_number                       = read_csv_file_column_number(file_name, delimiter_inp)
      self.data_array                          = read_csv_file_array(file_name, delimiter_inp, invalid_csv_str, invalid_element_str, *column_types)
      self.data_array_max_str                  = read_csv_file_array_max_str(file_name, delimiter_inp, invalid_csv_str, invalid_element_str)

   def align_string(self, data_array, data_array_max_str):
      return align_string_array(data_array, data_array_max_str)

   def __repr__(self):
      #print(self.data_array)
      #columns, columns_max_str = self.data_array
      aligned_str = self.align_string(self.data_array, self.data_array_max_str)
      #print(aligned_str)
      return '\n[ '                                + str(self.row_number) + \
             ' rows x '                            + str(self.column_number) + \
             ' columns ] \nFileName: '             + str(self.file_name) + \
             ', Delimiter: \"'                     + str(self.delimiter) + \
             '\", Invalid string in csv-file: \"'  + str(self.invalid_csv_str) + \
             '\", invalid string in dataframe: \"' + str(self.invalid_element_str) + \
             '\"'

# -------------------------------------------------------------------------------------------------
# Return an aligned string of an array:
def align_string_array(data_array, data_array_max_str):
   string = ""
   for ii,jj in data_array.items():
      string = string + ("{0:^"+str(data_array_max_str[ii])+"}").format(str(ii)) + "  "

   keys = []
   max_str_array = []
   for key in data_array:
      keys.append(key)
      max_str_array.append(data_array_max_str[key])

   string_array = []
   for ii in range(0, len(data_array[keys[0]])):
      string_array.append([])
      for jj in range(0, len(keys)):
         string_array[ii].append(data_array[keys[jj]][ii])

   aligned_string_array = outp.align_string_array(string_array, max_str_array)

   outp.output_string_array(aligned_string_array)

