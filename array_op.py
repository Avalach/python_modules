###################################################################################################
#                                                                                                 #
#     Array operations                                                                            #
#                                                                                                 #
###################################################################################################


from collections     import Counter


#--------------------------------------------------------------------------------------------------
# Making an array of a specific csv file columns only with valid values:
def x_y_array_dict(columns, column_name_1, column_name_2):
   x_array = []
   y_array = []
   for ii in range(0,len(columns[str(column_name_1)])):
      if ( (not columns[str(column_name_1)][ii] == "INVALID") and (not columns[str(column_name_2)][ii] == "INVALID") ):
         x_array.append(columns[str(column_name_1)][ii])
         y_array.append(columns[str(column_name_2)][ii])
   return x_array, y_array

#--------------------------------------------------------------------------------------------------
# Making an array by counting all elements of certain class names
def counter_array_dict(columns, column_name):
   class_counter = Counter()
   for element in columns[column_name]:
      class_counter[element] += 1
   return class_counter

