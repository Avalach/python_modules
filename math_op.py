###################################################################################################
#                                                                                                 #
#     Math operations based on NumPy and SciPy                                                    #
#                                                                                                 #
###################################################################################################

import numpy             as np
import matplotlib.pyplot as plt
import output            as outp


def random_n(N_number, sigma, mu):
   return sigma * np.random.randn(N_number) + mu
