###################################################################################################
#                                                                                                 #
#     Dataset operations based on module panda                                                    #
#                                                                                                 #
###################################################################################################

import yfinance          as yf
import matplotlib.pyplot as plt
import datetime


stocks=['^GDAXI','^MDAXI','^SDAXI','^TECDAX','^STOXX50E','^GSPC','^DJI','^N225']

start_date = datetime.datetime(2012,5,31)
end_date   = datetime.date.today()

stocks_df = yf.download(stocks, start_date, end_date)

print(stocks_df)

#print(stocks_df['GDAXI'])

stocks_df['High'].plot(title="DAX stock price")


#GDAXI_df = yf.download('^GDAXI')
#GDAXI_df['High'].plot(title="DAX stock price")

#MDAXI_df = yf.download('^MDAXI')
#MDAXI_df['High'].plot(title="MDAX stock price")

#SDAXI_df = yf.download('^SDAXI')
#SDAXI_df['High'].plot(title="SDAX stock price")

#TECDAX_df = yf.download('^TECDAX')
#TECDAX_df['High'].plot(title="TECDAX stock price")

#STOXX50E_df = yf.download('^STOXX50E')
#STOXX50E_df['High'].plot(title="STOXX50E stock price")

#GSPC_df = yf.download('^GSPC')
#GSPC_df['High'].plot(title="S%P stock price")

#DJI_df = yf.download('^DJI')
#DJI_df['High'].plot(title="Dow Jones stock price")

#N225_df = yf.download('^N225')
#N225_df['High'].plot(title="Nikkei stock price")





#tsla_df = yf.download('TSLA')
#tsla_df['High'].plot(title="Tesla stock price")



plt.show()

