###################################################################################################
#                                                                                                 #
#     Plot operations                                                                             #
#                                                                                                 #
###################################################################################################

import numpy             as np
import matplotlib
import matplotlib.pyplot as plt
import output            as outp
import math_op           as mop


def plot_marker(XX, YY, pointstyle, **kwargs):
   if type(pointstyle) == tuple:
      pointstyle = pointstyle[0]
   ax = plt.plot(XX, YY, pointstyle, **kwargs)
   return ax


def plot_marker_series(XX_list, YY_list, pointstyle_list, **kwargs):
   for ii in range(0,len(XX_list)):
      pointstyle = pointstyle_list[ii]
      ax = plt.plot(XX_list[ii], YY_list[ii], pointstyle, **kwargs)
   return ax


def plot_line(XX, YY, *args, **kwargs):
   ax = plt.plot(XX, YY, *args, **kwargs)
   return ax


def plot_line_series(XX_list, YY_list, *args, **kwargs_list):
   for ii in range(0,len(XX_list)):
      if kwargs_list:
         kwargs = kwargs_list[0][ii]
         ax = plt.plot(XX_list[ii], YY_list[ii], *args, **kwargs)
      else:
         ax = plt.plot(XX_list[ii], YY_list[ii])
   return ax


def plot_contour_lines(xx, yy, zz, *args, **kwargs):
   matplotlib.rcParams['contour.negative_linestyle'] = 'solid'
   if type(zz) == np.ndarray:
      XX = xx
      YY = yy
      ZZ = zz
   else:
      XX, YY = np.meshgrid(xx, yy)
      ZZ = []
      idx = 0
      for ii in range(0, len(xx)):
         aa = []
         for jj in range(0, len(yy)):
            aa.append(zz[idx])
            idx += 1
         ZZ.append(aa)
   return plt.contour(XX, YY, ZZ, *args, **kwargs)


def plot_contour_fill(xx, yy, zz, *args, **kwargs):
   matplotlib.rcParams['contour.negative_linestyle'] = 'solid'
   if type(zz) == np.ndarray:
      XX = xx
      YY = yy
      ZZ = zz
   else:
      XX, YY = np.meshgrid(xx, yy)
      ZZ = []
      idx = 0
      for ii in range(0, len(xx)):
         aa = []
         for jj in range(0, len(yy)):
            aa.append(zz[idx])
            idx += 1
         ZZ.append(aa)
   return plt.contourf(XX, YY, ZZ, *args, **kwargs)


def plot_contour_lines_and_fill(xx, yy, zz, *args, **kwargs):
   matplotlib.rcParams['contour.negative_linestyle'] = 'solid'
   if type(zz) == np.ndarray:
      XX = xx
      YY = yy
      ZZ = zz
   else:
      XX, YY = np.meshgrid(xx, yy)
      ZZ = []
      idx = 0
      for ii in range(0, len(xx)):
         aa = []
         for jj in range(0, len(yy)):
            aa.append(zz[idx])
            idx += 1
         ZZ.append(aa)
   kwargs_cl = []
   kwargs_cf = []
   if 'cl' in kwargs:
      kwargs_cl = kwargs['cl']
   if 'cf' in kwargs:
      kwargs_cf = kwargs['cf']
   if kwargs_cl:
      kwargs = kwargs_cl
   else:
      kwargs = {}
   cntr_lines = plt.contour(XX, YY, ZZ, *args, **kwargs)
   if kwargs_cf:
      kwargs = kwargs_cf
   else:
      kwargs = {}
   cntr_fill = plt.contourf(XX, YY, ZZ, *args, **kwargs)
   return cntr_lines, cntr_fill


def single_plot(plt_type, points_inp, values_inp, *args, **kwargs):
   if plt_type == 'marker':
      pointstyle = args
      return plot_marker(points_inp, values_inp, pointstyle, **kwargs)
   if plt_type == 'marker_series':
      pointstyle_list = args
      return plot_marker_series(points_inp, values_inp, pointstyle_list, **kwargs)
   if plt_type == 'line':
      return plot_line(points_inp, values_inp, *args, **kwargs)
   if plt_type == 'line_series':
      return plot_line_series(points_inp, values_inp, *args, **kwargs)
   if plt_type == 'contour_lines':
      return plot_contour_lines(points_inp[0], points_inp[1], values_inp, *args, **kwargs)
   if plt_type == 'contour_fill':
      return plot_contour_fill(points_inp[0], points_inp[1], values_inp, *args, **kwargs)
   if plt_type == 'contour_lines_and_fill':
      return plot_contour_lines_and_fill(points_inp[0], points_inp[1], values_inp, *args, **kwargs)


def multiple_plots(fig_nr, plt_nr, plt_rows, plt_columns, plt_type_list, points_inp_list, values_inp_list, args_list, kwargs_list):
   plt.figure(1,figsize=(9,10))
   ax = []
   for ii in range(0, plt_nr):
      ax.append(plt.subplot(plt_rows, plt_columns, ii+1))
      if (args_list[ii] and not kwargs_list[ii]):
         args = args_list[ii]
         single_plot(plt_type_list[ii], points_inp_list[ii], values_inp_list[ii], *args)
      elif (not args_list[ii] and kwargs_list[ii]):
         kwargs = kwargs_list[ii]
         single_plot(plt_type_list[ii], points_inp_list[ii], values_inp_list[ii], **kwargs)
      elif (args_list[ii] and kwargs_list[ii]):
         args = [args_list[ii]]
         kwargs = kwargs_list[ii]
         single_plot(plt_type_list[ii], points_inp_list[ii], values_inp_list[ii], *args, **kwargs)
      else:
         single_plot(plt_type_list[ii], points_inp_list[ii], values_inp_list[ii])
   plt.subplots_adjust(top=0.98, bottom=0.05, left=0.05, right=0.95, hspace=0.15, wspace=0.35)
   if not __name__ == "__main__":
      plt.show()





###################################################################################################
#                       Testing the module classes and functions                                  #
###################################################################################################

if __name__ == "__main__":

   plot_1D = False
   plot_2D = True
   plot_subplots = False

   def func(xx, yy):
      return 2*(np.exp(-xx**2-yy**2)-np.exp(-(xx-1)**2-(yy-1)**2))

   plot_points = []
   plot_values = []
   sample_points = []
   sample_values = []

   plot_points_mesh = [None]*2

   # Plot points for dimension 1:
   plot_points.append(np.arange(-3.0, 3.01, 0.05))
   sample_points.append(np.arange(-3.0, 3.01, 0.5))

   # Plot points for dimension 2:
   plot_points.append(np.arange(-3.0, 3.01, 0.05))
   sample_points.append(np.arange(-3.0, 3.01, 0.5))

   plot_points_mesh[0], plot_points_mesh[1] = np.meshgrid(plot_points[0], plot_points[1])

   plot_values.append(np.sin(plot_points[0]))
   plot_values.append(np.cos(plot_points[0]))
   plot_values.append(1/10*np.exp(plot_points[0]))

   plot_values_2D = []
   plot_points_2D = [None]*2
   plot_points_2D[0] = []
   plot_points_2D[1] = []
   for ii in plot_points[0]:
      for jj in plot_points[1]:
         plot_points_2D[0].append(ii)
         plot_points_2D[1].append(jj)
         plot_values_2D.append(func(ii,jj))

   points = np.random.rand(10, 2)
   values = func(points[:,0], points[:,1])

   sample_values.append(np.sin(sample_points[0]))
   sample_values.append(np.cos(sample_points[0]))
   sample_values.append(1/10*np.exp(sample_points[0]))

   plot_values_mesh = 2 * (  np.exp(-plot_points_mesh[0]**2 - plot_points_mesh[1]**2) \
                           - np.exp(-(plot_points_mesh[0] - 1)**2 - (plot_points_mesh[1] - 1)**2))



   if plot_1D:

      plt.figure(1,figsize=(9,10))

      ax = []

      ax.append(plt.subplot(3,2,1))
      plot_marker(sample_points[0], sample_values[0], '.')

      ax.append(plt.subplot(3,2,2))
      plot_marker_series([sample_points[0], sample_points[0], sample_points[0]], sample_values, ['.','o','+'])

      ax.append(plt.subplot(3,2,3))
      plot_line(plot_points[0], plot_values[0])

      ax.append(plt.subplot(3,2,4))
      plot_line_series([plot_points[0], plot_points[0], plot_points[0]], plot_values)

      ax.append(plt.subplot(3,2,5))
      plot_line(plot_points[0], plot_values[0], '--', color='y')

      ax.append(plt.subplot(3,2,6))
      plot_line_series([plot_points[0], plot_points[0], plot_points[0]], plot_values, [{'color':'y','linestyle':'--'},{'color':'g','linestyle':':'},{'color':'c','linestyle':'-'}])


   if plot_2D:

      plt.figure(2,figsize=(9,10))

      plt.ylim(0.0, 2*np.pi)
      plt.ylim(-1.2,1.2)

      ax = []
   
      ax.append(plt.subplot(3,2,1))
      cplt = plot_contour_lines(plot_points[0], plot_points[1], plot_values_2D, cmap="RdBu_r")
      plt.colorbar(cplt)

      ax.append(plt.subplot(3,2,2))
      cplt = plot_contour_lines(plot_points[0], plot_points[1], plot_values_2D, [-1.0,-0.4,-0.1,0,0.1,0.4,1.0], cmap="RdBu_r", linewidths=2, extend='both')
      ax[1].clabel(cplt, inline=1, fontsize=10, manual=[(0.0,-0.8),(0.0,-1.2),(0.0,-1.5),(1.4,1.4),(1.9,2.0),(2.0,2.3)])
      plt.colorbar(cplt)

      ax.append(plt.subplot(3,2,3))
      cplt = plot_contour_fill(plot_points[0], plot_points[1], plot_values_2D, cmap="RdBu_r")
      plt.colorbar(cplt)

      ax.append(plt.subplot(3,2,4))
      cplt = plot_contour_fill(plot_points[0], plot_points[1], plot_values_2D, 20, cmap="RdBu_r")
      plt.colorbar(cplt)

      ax.append(plt.subplot(3,2,5))
      cplt_lines, cplt_fill = plot_contour_lines_and_fill(plot_points[0], plot_points[1], plot_values_2D, cf={'cmap':"RdBu_r"})
      plt.colorbar(cplt_fill)
  
      ax.append(plt.subplot(3,2,6))
      cplt_lines, cplt_fill = plot_contour_lines_and_fill(plot_points[0], plot_points[1], plot_values_2D, 12, cl={'colors':'k','linewidths':0.5}, cf={'cmap':"RdBu_r"})
      plt.colorbar(cplt_fill)

      plt.subplots_adjust(top=0.98, bottom=0.05, left=0.05, right=0.95, hspace=0.15, wspace=0.35)


   if plot_subplots:

      #plt.figure(1,figsize=(9,10))

      #ax = []

      #ax.append(plt.subplot(3,2,1))
      #single_plot('marker', sample_points[0], sample_values[0], '.')

      #ax.append(plt.subplot(3,2,2))
      #single_plot('marker_series', [sample_points[0],sample_points[0],sample_points[0]], sample_values, ['.','o','+'])

      #ax.append(plt.subplot(3,2,3))
      #single_plot('line', plot_points[0], plot_values[0])

      #ax.append(plt.subplot(3,2,4))
      #single_plot('line_series', [plot_points[0],plot_points[0],plot_points[0]], plot_values)

      #ax.append(plt.subplot(3,2,5))
      #cplt = single_plot('contour_lines', plot_points_mesh, plot_values_mesh, cmap="RdBu_r")
      #plt.colorbar(cplt)

      #ax.append(plt.subplot(3,2,6))
      #cplt_lines, cplt_fill = single_plot('contour_lines_and_fill', plot_points_mesh, plot_values_mesh, 20, cl={'colors':'k','linewidths':0.5}, cf={'cmap':"RdBu_r"})
      #plt.colorbar(cplt_fill)

      #plt.subplots_adjust(top=0.98, bottom=0.05, left=0.05, right=0.95, hspace=0.15, wspace=0.35)


      plot_type_list  = ['marker', 'marker_series', 'line', 'line_series', 'contour_lines', 'contour_lines_and_fill']
      points_inp_list = [sample_points[0], [sample_points[0], sample_points[0], sample_points[0]], \
                         plot_points[0], [plot_points[0], plot_points[0], plot_points[0]], \
                         [plot_points[0],plot_points[1]], \
                         [plot_points[0],plot_points[1]]]
      values_inp_list = [sample_values[0], [sample_values[0], sample_values[1], sample_values[2]], \
                         plot_values[0], [plot_values[0], plot_values[1], plot_values[2]], plot_values_2D, plot_values_2D]
      args_list   = ['.', ['.','o','+'], '', '', '', 20]
      kwargs_list = ['', '', '', '', {'cmap':'RdBu_r'}, {'cl':{'colors':'k','linewidths':0.5},'cf':{'cmap':"RdBu_r"}}]
      multiple_plots(1, 6, 3, 2, plot_type_list, points_inp_list, values_inp_list, args_list, kwargs_list) 


   plt.show()
