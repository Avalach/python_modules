###################################################################################################
#                                                                                                 #
#     Dataset operations based on module panda                                                    #
#                                                                                                 #
###################################################################################################

import datetime
import numpy      as np
import pandas     as pd
import output     as outp
import general_op as gop
import slice_op   as slc

#--------------------------------------------------------------------------------------------------
# Reading  csv-file as panda dataframe:
#
# Input variables:
# file_name: type(str):       File name of the input csv-file
#
# Return variables:
# df:        type(dataframe): Dataframe of the csv-file
#

def read_csv_df(file_name, **kwargs):

   if "df_kwargs" in kwargs:
      df = pd.read_csv(file_name, **kwargs["df_kwargs"])
   else:
      df = pd.read_csv(file_name)
   if "set_freq" in kwargs:
      df = df.asfreq(pd.infer_freq(df.index))
   return df



#--------------------------------------------------------------------------------------------------
# Writing panda dataframe as csv-file:
#
# Input variables:
# df       : type(dataframe): Dataframe  
# file_name: type(str):       File name of the output csv-file
#
def write_csv_df(df, file_name):

   df.to_csv(file_name)



#--------------------------------------------------------------------------------------------------
# Get first row of a panda dataframe:
#
# Input variables:
# df:         type(dataframe): Dataframe
#
# Return variables:
# df.columns: type(list):      First row of the dataframe
#

def df_columns(df):

   return list(df.columns)



#--------------------------------------------------------------------------------------------------
# Get index column of a panda dataframe:
#
# Input variables:
# df:       type(dataframe): Dataframe
#
# Return variables:
# df.index: type(list):      First row of the dataframe
#

def df_rows(df):

   return list(df.index)



#--------------------------------------------------------------------------------------------------
# Get size of a panda dataframe:
#
# Input variables:
# df:       type(dataframe): Dataframe
#
# Return variables:
# df.size: type(int):        Size of the dataframe (all data elements)
#

def df_size(df):

   return df.size



#--------------------------------------------------------------------------------------------------
# Cut dataframe from top or bottom
#
# Input variables:
# df:      type(dataframe): Dataframe
# cut_dir: type(str):       Cut direction (top/bottom)
# cut_nr:  type(int):       Number of cutted rows
#
# Return variables:
# df_cut:  type(dataframe): Cutted dataframe
#

def cut_df(df, cut_dir, cut_nr):

   if (cut_dir == "top"):
      df_cut = df.head(cut_nr)
   elif (cut_dir == "bottom"):
      df_cut = df.tail(cut_nr)
   else:
      outp.message_line("Wrong statement in position 2 (choose top or bottom)!",headword="Fatal error")
   return df_cut



#--------------------------------------------------------------------------------------------------
# Transform dataframe to an numpy array:
#
# Input variables:
# df:       type(dataframe): Dataframe
#
# Return variables:
# df_numpy: type(list):      Numpy array of the dataframe
#

def df_numpy(df):

   df_numpy = df.to_numpy()
   return df_numpy



#--------------------------------------------------------------------------------------------------
# Fast statistic of the dataframe
#
# Input variables:
# df:                type(dataframe): Dataframe
#
# Return variables:
# df_fast_statistic: type(dataframe): Dataframe of a rough statistic overview of the dataset
#

def df_fast_statistic(df):

   df_fast_statistic = df.describe()
   return df_fast_statistic



#--------------------------------------------------------------------------------------------------
# Transposing the dataframe 
#
# Input variables:
# df:           type(dataframe): Dataframe
#
# Return variables:
# df_transpose: type(dataframe): Transposed dataframe
#

def df_transpose(df):

   df_transpose = df.T
   return df_transpose



#--------------------------------------------------------------------------------------------------
# Sorting dataframe by column index or column name
#
# Input variables:
# df:          type(dataframe): Dataframe
# sort_option: type(str/int):   Option for sorting the dataframe entries (string for column name or integer for column indices (0=index, 1=column)
#
# Return variables:
# df_sorted:   type(dataframe): Sorted dataframe
#

def sort_df(df, sort_option, **kwargs):

   if (type(sort_option) == int):   # Sorting the dataset by the given column integer index
      if "sort_kwargs" in kwargs:
         df_sorted = df.sort_index(axis=sort_option,**kwargs["sort_kwargs"])
      else:
         df_sorted = df.sort_index(axis=sort_option)
   elif (type(sort_option) == str):   # Sorting the dataset by the given row string index
      if "sort_kwargs" in kwargs:
         df_sorted = df.sort_values(by=str(sort_option),**kwargs["sort_kwargs"])
      else:
         df_sorted = df.sort_values(by=str(sort_option))
   else:
      outp.message_line_array(["Wrong type in position 2!","It has to be type str or int!"],headword="Fatal error")
   return df_sorted



#--------------------------------------------------------------------------------------------------
# Get row labels and column indices of a panda dataframe which have the same value:
def get_rows_columns_by_value(df, value):

   label_list = []
   for ii in df.index:
      for jj in df.columns:
         if (df.loc[ii,jj] == value):
            label_list.append([ii,jj])
   return label_list



#--------------------------------------------------------------------------------------------------
# Get certain colomn of a panda dataframe chosen by first column name as index:
#
# Input variables:
# df:             type(dataframe): Dataframe
# rows_select:    type(list):      Input list to select the rows - rows_select=[string,type]
# columns_select: type(list):      Input list to select the columns - columns_select=[string,type] 
#
# Return variables:
# df.slice:       type(dataframe): Sliced dataframe
#

def df_edit(df, rows_slice, columns_slice, edit_op, *args):

   #--------------------------------------------------------------------------------------------------
   # Subfunction: get dataframe values
   #
   def df_edit_get(df, rows_slice, columns_slice):

      rows_slice = slice(rows_slice.start-1, rows_slice.stop, rows_slice.step)
      columns_slice = slice(columns_slice.start-1, columns_slice.stop, columns_slice.step)
      df_mod = df.iloc[rows_slice, columns_slice]
      return df_mod

   #--------------------------------------------------------------------------------------------------
   # Subfunction: get dataframe values
   #
   def df_edit_set(df, rows_slice, columns_slice, args):

      df_mod = df.copy()
      rows_slice = slice(rows_slice.start-1, rows_slice.stop, rows_slice.step)
      columns_slice = slice(columns_slice.start-1, columns_slice.stop, columns_slice.step)
      df_mod.iloc[rows_slice, columns_slice] = args[0]
      return df_mod

   #--------------------------------------------------------------------------------------------------
   # Subfunction: Filter slice of a dataframe by certain Boolean condition
   #
   # Input variables:
   # df:          type(dataframe): Dataframe
   # column_name: type(str):       string name of a dataframe column
   # filter_list: type(list):      Filter list with relational operators or index labels
   #
   # Return variables:
   # df_filtered: type(dataframe): Sorted dataframe
   #
   def df_filter_slice(df, rows_slice, columns_slice, filter_list):

      df_mod = df.copy()
      rows_slice = slice(rows_slice.start-1, rows_slice.stop, rows_slice.step)
      columns_slice = slice(columns_slice.start-1, columns_slice.stop, columns_slice.step)
      if (filter_list[0][0] == "=="):
        df_boolean = df_mod.iloc[rows_slice, columns_slice] == filter_list[0][1]
        label_list = get_rows_columns_by_value(df_boolean, False)
        for ii in label_list:
            df_mod.loc[ii[0],ii[1]] = np.nan
      elif (filter_list[0][0] == ">"):
        df_boolean = df_mod.iloc[rows_slice, columns_slice] > filter_list[0][1] 
        label_list = get_rows_columns_by_value(df_boolean, False)
        for ii in label_list:
           df_mod.loc[ii[0],ii[1]] = np.nan
      elif (filter_list[0][0] == "<"):
        df_boolean = df_mod.iloc[rows_slice, columns_slice] < filter_list[0][1]
        label_list = get_rows_columns_by_value(df_boolean, False)
        for ii in label_list:
           df_mod.loc[ii[0],ii[1]] = np.nan
      return df_mod

   #--------------------------------------------------------------------------------------------------
   # Subfunction: Filter column of a dataframe by certain Boolean condition
   #
   # Input variables:
   # df:          type(dataframe): Dataframe
   # column_name: type(str):       string name of a dataframe column
   # filter_list: type(list):      Filter list with relational operators or index labels
   #
   # Return variables:
   # df_filtered: type(dataframe): Sorted dataframe
   #
   def df_filter_column(df, rows_slice, columns_slice, filter_list):

      df_mod = df.copy()
      rows_slice = slice(rows_slice.start-1, rows_slice.stop, rows_slice.step)
      columns_slice = slice(columns_slice.start-1, columns_slice.stop, columns_slice.step)
      if (columns_slice.start-columns_slice.stop > 1):
         outp.message_line("Only column slice with one label can be used!",headword="Fatal error")
      if (filter_list[0][0] == "=="):
        df_mod = df[df[df.columns[columns_slice][0]] == filter_list[0][1]]
      elif (filter_list[0][0] == ">"):
        df_mod = df[df[df.columns[columns_slice][0]] > filter_list[0][1]]
      elif (filter_list[0][0] == "<"):
        df_mod = df[df[df.columns[columns_slice][0]] < filter_list[0][1]]
      return df_mod

   #--------------------------------------------------------------------------------------------------
   # Subfunction: Filter row of a dataframe by certain Boolean condition
   #
   # Input variables:
   # df:          type(dataframe): Dataframe
   # column_name: type(str):       string name of a dataframe column
   # filter_list: type(list):      Filter list with relational operators or index labels
   #
   # Return variables:
   # df_filtered: type(dataframe): Sorted dataframe
   #
   def df_filter_row(df, rows_slice, columns_slice, filter_list):

      rows_slice = slice(rows_slice.start-1, rows_slice.stop, rows_slice.step)
      columns_slice = slice(columns_slice.start-1, columns_slice.stop, columns_slice.step)
      columns = []
      if (rows_slice.start-rows_slice.stop > 1):
         outp.message_line("Only column slice with one label can be used!",headword="Fatal error")
      row     = df.index[rows_slice][0]
      columns = []
      data    = []
      if (filter_list[0][0] == "=="):
         idx = 0
         for ii in df.loc[df.index[rows_slice][0],:]:
            if ii == filter_list[0][1]:
               columns.append(df.columns[idx][0])
               data.append(ii)
            idx += 1
         df_mod = pd.DataFrame([data], index=[row], columns=columns)
      elif (filter_list[0][0] == ">"):
         idx = 0
         for ii in df.loc[df.index[rows_slice][0],:]:
            if ii > filter_list[0][1]:
               columns.append(df.columns[idx][0])
               data.append(ii)
            idx += 1
         df_mod = pd.DataFrame([data], index=[row], columns=columns)
      elif (filter_list[0][0] == "<"):
         idx = 0
         for ii in df.loc[df.index[rows_slice][0],:]:
            if ii < filter_list[0][1]:
               columns.append(df.columns[idx][0])
               data.append(ii)
            idx += 1
         df_mod = pd.DataFrame([data], index=[row], columns=columns)
      return df_mod

   #---------------------------------------------------------------------------
   # Main function:
   if (edit_op == "get"):
      df_mod = df_edit_get(df, rows_slice, columns_slice)
   if (edit_op == "set" and args):
      df_mod = df_edit_set(df, rows_slice, columns_slice, args)
   if (edit_op == "filter_slice" and args):
      df_mod = df_filter_slice(df, rows_slice, columns_slice, args)
   if (edit_op == "filter_column" and args):
      df_mod = df_filter_column(df, rows_slice, columns_slice, args)
   if (edit_op == "filter_row" and args):
      df_mod = df_filter_row(df, rows_slice, columns_slice, args)
   return df_mod



#--------------------------------------------------------------------------------------------------
# Set dataframe element
#
# Input variables:
# df:      type(dataframe): Dataframe
# indices: type(str/int):   Cut direction (top/bottom)
# element: type(...):       Number of cutted rows
#
# Return variables:
# df_mod:  type(dataframe): Updated dataframe
#

def df_set(df, indices, element):
   
   df_mod = df.copy()
   if (type(indices[0]) == int and type(indices[1]) == int):
      df_mod.iat[indices[0],indices[1]] = element
      return df_mod
   elif (type(indices[0]) == str and type(indices[1]) == str):
      df_mod.at[indices[0],indices[1]] = element
      return df_mod
   else:
      outp.message_line("Indices list has to be two integers or strings!",headword="Fatal error")





###################################################################################################
#                       Testing the module classes and functions                                  #
###################################################################################################

if __name__ == "__main__":

   import sys

   outp.message_empty_line()
   outp.message_box_array(["","Python and Pandas version",""], 100, align="center")
   outp.message_empty_line()
   outp.message_line("Python version:")
   outp.output_object(sys.version)
   outp.message_empty_line()
   outp.message_line("Version info:")
   outp.output_object(sys.version_info)
   outp.message_empty_line()
   outp.message_line("System path:")
   outp.output_object(sys.path)
   outp.message_empty_line()
   outp.message_line("Pandas version:")
   outp.output_object(pd.__version__)
   outp.message_empty_line()
   outp.message_empty_line()
   outp.message_empty_line()
   outp.message_box_array(["", "Testing dataset operation functions",""], 100, align="center")
   outp.message_empty_line()
   outp.message_empty_line()  

   #dates = pd.date_range('20130101', periods=6)
   #df1 = pd.DataFrame(np.random.randn(6, 4), index=dates, columns=list('ABCD'))
   #outp.message_empty_line()
   #print(df1)
   #df1.to_csv('test_df1.csv')

   df1 = read_csv_df('test_df1.csv', df_kwargs={"dtype":{"A":"float64","B":"float64","C":"float64","D":"float64"}, \
                            "index_col":0, "parse_dates":[0], "date_parser":pd.to_datetime}, set_freq="True")

   #df2 = pd.DataFrame({'A': 1., \
   #                    'B': pd.Timestamp('20130102'), \
   #                    'C': pd.Series(1, index=list(range(4)), dtype='float32'), \
   #                    'D': np.array([3] * 4, dtype='int32'), \
   #                    'E': pd.Categorical(["test", "train", "test", "train"]), \
   #                    'F': 'foo'})
   #print(df2)
   #df2.to_csv('test_df2.csv')

   df2 = read_csv_df('test_df2.csv', df_kwargs={"dtype":{"A":"float64","B":"str","C":"float32","D":"int32","D":"category","E":"object"}, \
                            "index_col":0, "parse_dates":["B"], "date_parser":pd.to_datetime})

   outp.message_empty_line()
   outp.message_box_array(["Testing function: read_csv_df('test_df1.csv', df_kwargs={...}, set_freq=\"True\""],100)
   outp.message_empty_line()
   outp.output_object(df1)
   outp.message_empty_line()
   outp.output_object(df2)
   outp.message_empty_line()
   outp.output_object(df1.dtypes)
   outp.message_empty_line()
   outp.output_object(df2.dtypes)
   outp.message_empty_line()
   outp.message_empty_line()


   outp.message_empty_line()
   outp.message_box_array(["cut_df(df, cut_dir, cut_nr)"],100)
   outp.message_empty_line()
   outp.output_object(cut_df(df1,"top",3),comment_line="Out:")
   outp.message_empty_line()
   outp.output_object(cut_df(df2,"bottom",2))
   outp.message_empty_line()
   outp.message_empty_line()


   outp.message_empty_line()
   outp.message_box_array(["df_columns(df)"],100)
   outp.message_empty_line()
   outp.output_object(df_columns(df1))
   outp.message_empty_line()
   outp.message_empty_line()


   outp.message_empty_line()
   outp.message_box_array(["df_rows(df)"],100)
   outp.message_empty_line()
   outp.output_object(df_rows(df1))
   outp.message_empty_line()
   outp.message_empty_line()


   outp.message_empty_line()
   outp.message_box_array(["df_numpy(df)"],100)
   outp.message_empty_line()
   df1_numpy = df_numpy(df1)
   outp.output_object(df1_numpy)
   outp.message_empty_line()
   df2_numpy = df_numpy(df2)
   outp.output_object(df2_numpy)
   outp.message_empty_line()
   outp.message_empty_line()


   outp.message_empty_line()
   outp.message_box_array(["df_fast_statistic(df)"],100)
   outp.message_empty_line()
   df1_fs = df_fast_statistic(df1)
   outp.output_object(df1_fs)
   outp.message_empty_line()
   df2_fs = df_fast_statistic(df2)
   outp.output_object(df2_fs)
   outp.message_empty_line()
   outp.message_empty_line()


   outp.message_empty_line()
   outp.message_box_array(["df_transpose(df)"],100)
   outp.message_empty_line()
   df1_t = df_transpose(df1)
   outp.output_object(df1_t)
   outp.message_empty_line()
   df2_t = df_transpose(df2)
   outp.output_object(df2_t)
   outp.message_empty_line()
   outp.message_empty_line()


   outp.message_empty_line()
   outp.message_box_array(["sort_df(df, sort_option, **kwargs)"],100)
   outp.message_empty_line()
   df1_sorted = sort_df(df1,0,sort_kwargs={"ascending":False})
   outp.output_object(df1_sorted)
   outp.message_empty_line()
   df1_sorted = sort_df(df1,1,sort_kwargs={"ascending":False})
   outp.output_object(df1_sorted)
   outp.message_empty_line()
   df1_sorted = sort_df(df1,'B')
   outp.output_object(df1_sorted)
   outp.message_empty_line()
   outp.message_empty_line()


   outp.message_empty_line()
   outp.message_box_array(["Testing str_slice(test_list_string) and int_slice(test_list_int) silce functions "],100)
   outp.message_empty_line()
   #---------------------------------------------------------------------------

   # Test of string and integer slicing
   test_list_string = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L"]
   test_list_int = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]
   print(test_list_string)
   print(test_list_int)
   print(slc.str_to_int_idx(test_list_string))
   print(slc.str_to_int_idx(test_list_int))

   print(slc.str_slice(test_list_string))
   print(slc.int_slice(test_list_int))
   print(slc.str_slice(test_list_string, "D"))
   print(slc.int_slice(test_list_int, 4))
   print(slc.str_slice(test_list_string, "D", "I"))
   print(slc.int_slice(test_list_int, 4, 9))

   outp.message_empty_line()
   outp.message_empty_line()



   #---------------------------------------------------------------------------
   outp.message_empty_line()
   outp.message_box_array(["def df_edit(df, rows_select, columns_select, 'get')"],100)
   outp.message_empty_line()

   # Get a slice of the dataframe:
   outp.message_line("Get a slice of the dataframe by string or by integer labeling:")
   outp.message_empty_line()
   print(df1)
   outp.message_empty_line()
   rows_list = list(df1.index.strftime("%Y-%m-%d"))
   rows_slice = slc.str_slice(rows_list, '2013-01-02', '2013-01-05')
   columns_list = df_columns(df1)
   columns_slice = slc.str_slice(columns_list, 'B', 'C')
   df1_mod = df_edit(df1, rows_slice, columns_slice, "get")
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   rows_list = list(df1.index.strftime("%Y-%m-%d"))
   rows_slice = slc.str_slice(rows_list)
   columns_list = df_columns(df1)
   columns_slice = slc.str_slice(columns_list)
   df1_mod = df_edit(df1, rows_slice, columns_slice, "get")
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   rows_list = list(df1.index.strftime("%Y-%m-%d"))
   rows_slice = slc.str_slice(rows_list,'2013-01-04')
   columns_list = df_columns(df1)
   columns_slice = slc.str_slice(columns_list)
   df1_mod = df_edit(df1, rows_slice, columns_slice, "get")
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   rows_list = list(df1.index.strftime("%Y-%m-%d"))
   rows_slice = slc.str_slice(rows_list)
   columns_list = df_columns(df1)
   columns_slice = slc.str_slice(columns_list,'B')
   df1_mod = df_edit(df1, rows_slice, columns_slice, "get")
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   df1_mod = df_edit(df1, slice(2,5), slice(2,3), "get")
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()


   #---------------------------------------------------------------------------
   outp.message_empty_line()
   outp.message_box_array(["def df_edit(df, rows_select, columns_select, 'set', array)"],100)
   outp.message_empty_line()

   rows_list = list(df1.index.strftime("%Y-%m-%d"))
   rows_slice = slc.str_slice(rows_list, '2013-01-02', '2013-01-05')
   columns_list = df_columns(df1)
   columns_slice = slc.str_slice(columns_list, 'B', 'C')
   array = [['1.0','2.0'],['3.0','4.0'],['5.0','6.0'],['7.0','8.0']]
   df1_mod = df_edit(df1, rows_slice, columns_slice, "set", array)
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   rows_list = list(df1.index.strftime("%Y-%m-%d"))
   rows_slice = slc.str_slice(rows_list)
   columns_list = df_columns(df1)
   columns_slice = slc.str_slice(columns_list)
   array = [list(range(0,4)),list(range(0,4)),list(range(0,4)),list(range(0,4)),list(range(0,4)),list(range(0,4))]
   df1_mod = df_edit(df1, rows_slice, columns_slice, "set", array)
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   rows_list = list(df1.index.strftime("%Y-%m-%d"))
   rows_slice = slc.str_slice(rows_list, '2013-01-04')
   columns_list = df_columns(df1)
   columns_slice = slc.str_slice(columns_list)
   array = ['1.0','2,0','3.0','4.0']
   df1_mod = df_edit(df1, rows_slice, columns_slice, "set", array)
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   rows_list = list(df1.index.strftime("%Y-%m-%d"))
   rows_slice = slc.str_slice(rows_list)
   columns_list = df_columns(df1)
   columns_slice = slc.str_slice(columns_list,'B')
   array = [['1.0'],['2.0'],['3.0'],['4.0'],['5.0'],['6.0']]
   df1_mod = df_edit(df1, rows_slice, columns_slice, "set", array)
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   array = [['1.0','2.0'],['3.0','4.0'],['5.0','6.0'],['7.0','8.0']]
   df1_mod = df_edit(df1, slice(2,5), slice(2,3), "set", array)
   outp.output_object(df1_mod,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()


   #---------------------------------------------------------------------------
   outp.message_empty_line()
   outp.message_box_array(["def df_edit(df, rows_select, columns_select, 'filter_slice', filter_condition)"],100)
   outp.message_empty_line()

   rows_list = list(df1.index.strftime("%Y-%m-%d"))
   rows_slice = slc.str_slice(rows_list, '2013-01-02', '2013-01-05')
   columns_list = df_columns(df1)
   columns_slice = slc.str_slice(columns_list, 'B', 'C')
   df1_filter = df_edit(df1, rows_slice, columns_slice, "filter_slice", [">",0.0])
   outp.output_object(df1_filter,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   rows_list = list(df1.index.strftime("%Y-%m-%d"))
   rows_slice = slc.str_slice(rows_list, '2013-01-02', '2013-01-05')
   columns_list = df_columns(df1)
   columns_slice = slc.str_slice(columns_list, 'B', 'C')
   df1_filter = df_edit(df1, rows_slice, columns_slice, "filter_slice", ["<",0.0])
   outp.output_object(df1_filter,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   rows_list = list(df1.index.strftime("%Y-%m-%d"))
   rows_slice = slc.str_slice(rows_list)
   columns_list = df_columns(df1)
   columns_slice = slc.str_slice(columns_list, 'B')
   df1_filter = df_edit(df1, rows_slice, columns_slice, "filter_slice", [">",0.0])
   outp.output_object(df1_filter,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   rows_list = list(df1.index.strftime("%Y-%m-%d"))
   rows_slice = slc.str_slice(rows_list)
   columns_list = df_columns(df1)
   columns_slice = slc.str_slice(columns_list, 'B')
   df1_filter = df_edit(df1, rows_slice, columns_slice, "filter_slice", ["<",0.0])
   outp.output_object(df1_filter,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   rows_list = list(df1.index.strftime("%Y-%m-%d"))
   rows_slice = slc.str_slice(rows_list, '2013-01-02', '2013-01-05')
   columns_list = df_columns(df1)
   columns_slice = slc.str_slice(columns_list)
   df1_filter = df_edit(df1, rows_slice, columns_slice, "filter_slice", [">",0.0])
   outp.output_object(df1_filter,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   rows_list = list(df1.index.strftime("%Y-%m-%d"))
   rows_slice = slc.str_slice(rows_list, '2013-01-02', '2013-01-05')
   columns_list = df_columns(df1)
   columns_slice = slc.str_slice(columns_list)
   df1_filter = df_edit(df1, rows_slice, columns_slice, "filter_slice", ["<",0.0])
   outp.output_object(df1_filter,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()


   #---------------------------------------------------------------------------
   outp.message_empty_line()
   outp.message_box_array(["def df_edit(df, rows_select, columns_select, 'filter_column', filter_condition)"],100)
   outp.message_empty_line()

   rows_list = list(df1.index.strftime("%Y-%m-%d"))
   rows_slice = slc.str_slice(rows_list)
   columns_list = df_columns(df1)
   columns_slice = slc.str_slice(columns_list, 'B')
   df1_filter = df_edit(df1, rows_slice, columns_slice, "filter_column", [">",0.0])
   outp.output_object(df1_filter,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   rows_list = list(df1.index.strftime("%Y-%m-%d"))
   rows_slice = slc.str_slice(rows_list)
   columns_list = df_columns(df1)
   columns_slice = slc.str_slice(columns_list, 'B')
   df1_filter = df_edit(df1, rows_slice, columns_slice, "filter_column", ["<",0.0])
   outp.output_object(df1_filter,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()


   #---------------------------------------------------------------------------
   outp.message_empty_line()
   outp.message_box_array(["def df_edit(df, rows_select, columns_select, 'filter_row', filter_condition)"],100)
   outp.message_empty_line()

   rows_list = list(df1.index.strftime("%Y-%m-%d"))
   rows_slice = slc.str_slice(rows_list, '2013-01-02')
   columns_list = df_columns(df1)
   columns_slice = slc.str_slice(columns_list)
   df1_filter = df_edit(df1, rows_slice, columns_slice, "filter_row", [">",0.0])
   outp.output_object(df1_filter,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

   rows_list = list(df1.index.strftime("%Y-%m-%d"))
   rows_slice = slc.str_slice(rows_list, '2013-01-02')
   columns_list = df_columns(df1)
   columns_slice = slc.str_slice(columns_list)
   df1_filter = df_edit(df1, rows_slice, columns_slice, "filter_row", ["<",0.0])
   outp.output_object(df1_filter,comment_line="Out:")
   outp.message_empty_line()
   outp.message_empty_line()

