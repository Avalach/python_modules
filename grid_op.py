###################################################################################################
#                                                                                                 #
#     Grid operations                                                                             #
#                                                                                                 #
###################################################################################################

import numpy             as np
import itertools
 


def meshgrid(xx, yy):
   return np.meshgrid(xx,yy)


def inverse_meshgrid(xx,yy):
   return [np.transpose(np.meshgrid(xx,yy)[0]),np.transpose(np.meshgrid(xx,yy)[1])]
   #return np.transpose(np.array(np.meshgrid(xx,yy)))


def meshgrid_to_axis(zz):
   xx = zz[0,:]
   yy = zz[:,0]
   return xx, yy


def combination_vector(vector_list):
   return list(itertools.product(*vector_list))
      




###################################################################################################
#                       Testing the module classes and functions                                  #
###################################################################################################

if __name__ == "__main__":

   xx = np.arange(0,3)
   yy = np.arange(4,9)

   mg = meshgrid(xx,yy)
   print(mg)

   print()
   print()

   mginv = inverse_meshgrid(xx,yy)
   print(mginv)

   print()
   print()

   mgrid = np.mgrid[0:3,4:9]
   print(mgrid)
