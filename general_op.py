

###################################################################################################
#                                                                                                 #
#     Dataset operations based on module panda                                                    #
#                                                                                                 #
###################################################################################################



#--------------------------------------------------------------------------------------------------
# Counts all items inside nested lists
#
# Input variables:
# nested_lists:         type(list): Dataframe
#
# Return variables:
# sum_items: type(int): Number of all items inside nested lists
#

def all_items_number(nested_lists):
    if type(nested_lists) == list:
        return sum(all_items_number(subitem) for subitem in nested_lists)
    else:
        return 1
