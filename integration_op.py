###################################################################################################
#                                                                                                 #
#     Integration operations                                                                      #
#                                                                                                 #
###################################################################################################

import numpy             as np
import matplotlib.pyplot as plt
import output            as outp
import math_op           as mop
import itertools

from   scipy      import integrate


def integration(method, rr_axis, lower_limits, upper_limits, function, *function_parameters, **kwargs):

   if method == 'quad':
      func_parameters = ()
      for ii in range(0, len(function_parameters)):
         func_parameters = func_parameters + (function_parameters[ii],)
      if len(func_parameters) > 0:
         integration_out = integrate.quad(function, lower_limits[0], upper_limits[0], args=func_parameters, **kwargs)
      else:
         integration_out = integrate.quad(function, lower_limits[0], upper_limits[0], **kwargs)

   if method == 'fixed_quad':
      func_parameters = ()
      for ii in range(0, len(function_parameters)):
         func_parameters = func_parameters + (function_parameters[ii],)
      if len(func_parameters) > 0:
         integration_out = integrate.fixed_quad(function, lower_limits[0], upper_limits[0], args=func_parameters, **kwargs)
      else:
         integration_out = integrate.fixed_quad(function, lower_limits[0], upper_limits[0], **kwargs)

   if method == 'quadrature':
      func_parameters = ()
      for ii in range(0, len(function_parameters)):
         func_parameters = func_parameters + (function_parameters[ii],)
      if len(func_parameters) > 0:
         integration_out = integrate.quadrature(function, lower_limits[0], upper_limits[0], args=func_parameters, **kwargs)
      else:
         integration_out = integrate.quadrature(function, lower_limits[0], upper_limits[0], **kwargs)

   if method == 'dblquad':
      func_parameters = ()
      for ii in range(0, len(function_parameters)):
         func_parameters = func_parameters + (function_parameters[ii],)
      if len(func_parameters) > 0:
         integration_out = integrate.dblquad(function, lower_limits[0], upper_limits[0], lower_limits[1], upper_limits[1], \
                                             args=func_parameters, **kwargs)
      else:
         integration_out = integrate.dblquad(function, lower_limits[0], upper_limits[0], lower_limits[1], upper_limits[1], \
                                             **kwargs)

   if method == 'tplquad':
      func_parameters = ()
      for ii in range(0, len(function_parameters)):
         func_parameters = func_parameters + (function_parameters[ii],)
      if len(func_parameters) > 0:
         integration_out = integrate.tplquad(function, lower_limits[0], upper_limits[0], lower_limits[1], upper_limits[1], \
                                             lower_limits[2], upper_limits[2], args=func_parameters, **kwargs)
      else:
         integration_out = integrate.tplquad(function, lower_limits[0], upper_limits[0], lower_limits[1], upper_limits[1], \
                                             lower_limits[2], upper_limits[2], **kwargs) 

   return integration_out





###################################################################################################
#                       Testing the module classes and functions                                  #
###################################################################################################


if __name__ == "__main__":


   integration_1D = True
   integration_2D = False


   def scalar_test_function_1D(xx, *args):
      xx_0  = args[0]
      sigma = args[1]
      ff    = np.exp(-((xx-xx_0)/(np.sqrt(2)*sigma))**2) 
      return ff


   def scalar_test_function_2D(xx, yy, *args):
      #print(args)
      xx_0    = args[0]
      yy_0    = args[1]
      sigma_x = args[2]
      sigma_y = args[3]
      ff      =  np.exp(-((xx-xx_0)/(np.sqrt(2)*sigma_x))**2) * np.exp(-((yy-yy_0)/(np.sqrt(2)*sigma_y))**2)
      return ff


   def scalar_test_function_2D_test(x, y):
      ff      =  np.exp(-x**2) * np.exp(-y**2)
      return ff


   def scalar_test_function_3D(xx, yy, zz, *args):
      xx_0    = args[0]
      yy_0    = args[1]
      zz_0    = args[2]
      sigma_x = args[3]
      sigma_y = args[4]
      sigma_z = args[5]
      ff      =   np.exp(-((xx-xx_0)/(np.sqrt(2)*sigma_x))**2) * np.exp(-((yy-yy_0)/(np.sqrt(2)*sigma_y))**2) \
                * np.exp(-((zz-zz_0)/(np.sqrt(2)*sigma_z))**2)
      return ff


   xx = np.linspace(0, 10, 1000)
   yy = np.linspace(0,  5,  50)
   zz = np.linspace(0, 20, 400)

   print()
   integration_value = integration('quad', xx, [0.0], [10.0], scalar_test_function_1D, 5.0, 1.5)
   print(integration_value)
   print()
   integration_value = integration('fixed_quad', xx, [0.0], [10.0], scalar_test_function_1D, 5.0, 1.5, n=10)
   print(integration_value)
   print()
   integration_value = integration('quadrature', xx, [0.0], [10.0], scalar_test_function_1D, 5.0, 1.5, tol=1e-6)
   print(integration_value)
   print()
   integration_value = integration('dblquad', xx, [0.0, lambda x: 0.0], [10.0, lambda x: 5.0], scalar_test_function_2D, \
                                   5.0, 1.5, 2.5, 1.2)
   print(integration_value)
   print()
   integration_value = integration('tplquad', xx, [0.0, lambda x: 0.0, lambda x, y: 0.0], [10.0, lambda x: 5.0, lambda x, y: 20.0], \
                                    scalar_test_function_3D, 5.0, 1.5, 2.5, 1.2, 10.0, 3.0)
   print(integration_value)
   print()

