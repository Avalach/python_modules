###################################################################################################
#                                                                                                 #
#     Derivative operations                                                                       #
#                                                                                                 #
###################################################################################################

import numpy             as np
import matplotlib.pyplot as plt
import output            as outp
import math_op           as mop
import plot_op           as plo
import itertools

from findiff import FinDiff


#def derivative_bd(xx, yy, dim, order, accuracy):
#   delta_xx = (xx[1] - xx[0])
#   xx_bd = np.concatenate((np.zeros(accuracy), xx, np.zeros(accuracy)))
#   yy_bd = np.concatenate((np.zeros(accuracy), yy, np.zeros(accuracy)))
#   yy_der_bd = FinDiff(dim-1, delta_xx, order, acc=accuracy)(yy_bd)
#   yy_der = np.split(yy_der_bd, [accuracy,accuracy+len(yy)])
#   return yy_der[1]


def derivative(rr_axis, derivative_args, accuracy, function, *function_args):
   delta_rr = [ abs(rr[ii][1]-rr[ii][0]) for ii in range(0, len(rr)) ]
   derivative_args_ = []
   if (isinstance(rr_axis[0],np.ndarray)):
      rr_meshgrid = np.meshgrid(*rr_axis, indexing='ij')
   else:
      rr_meshgrid = [rr_axis]
   args = ()
   for ii in range(0, len(rr_meshgrid)):
      args = args + (rr_meshgrid[ii],)
   for ii in range(0, len(function_args)):
      args = args + (function_args[ii],)
   ff_meshgrid = function(*args)
   for ii in range(0, len(derivative_args)):
      ii_args = []
      ii_args.append(derivative_args[ii][0]-1)
      ii_args.append(delta_rr[derivative_args[ii][0]-1])
      ii_args.append(derivative_args[ii][1])
      ii_args = tuple(ii_args)
      derivative_args_.append(ii_args)
   args = tuple(derivative_args_)
   ff_meshgrid_der = FinDiff(*args, acc=accuracy)(ff_meshgrid)
   if (isinstance(rr_axis[0],np.ndarray)):
      ff_meshgrid_der = ff_meshgrid_der.swapaxes(0,1)
   return ff_meshgrid_der


# TODO cut out of ff_meshgrid_bd
def derivative_bd(rr_axis, derivative_args, accuracy, function, *function_args):
   delta_rr = [ abs(rr[ii][1]-rr[ii][0]) for ii in range(0, len(rr)) ]
   derivative_args_ = []
   rr_axis_bd = []
   for ii in range(0, len(rr_axis)):
      rr_axis_bd.append(np.concatenate((np.zeros(accuracy), rr_axis[ii], np.zeros(accuracy))))
   rr_meshgrid_bd = np.meshgrid(*rr_axis_bd)
   ff_meshgrid_bd = function(rr_meshgrid_bd, *function_args)
   for ii in range(0, len(derivative_args)):
      ii_args = []
      ii_args.append(derivative_args[ii][0]-1)
      ii_args.append(delta_rr[derivative_args[ii][0]-1])
      ii_args.append(derivative_args[ii][1])
      ii_args = tuple(ii_args)
      derivative_args_.append(ii_args)
   args = tuple(derivative_args_)
   ff_meshgrid_der_bd = FinDiff(*args, acc=accuracy)(ff_meshgrid_bd)
   print(ff_meshgrid_der_bd)
   return ff_meshgrid_der





###################################################################################################
#                       Testing the module classes and functions                                  #
###################################################################################################


if __name__ == "__main__":

   derivatives_1D = True
   derivatives_2D = True


   def scalar_test_function_1D(xx, *args):
      xx_0  = args[0]
      sigma = args[1]
      omega = args[2]
      ff    = np.exp(-((xx-xx_0)/(np.sqrt(2)*sigma))**2) * np.cos(omega*xx)
      return ff


   def scalar_test_function_1D_derivative_d_dx(xx, *args):
      xx_0  = args[0]
      sigma = args[1]
      omega = args[2]
      ff    = - ( omega*sigma**2 * np.sin(omega*xx) + xx*np.cos(omega*xx) - xx_0*np.cos(omega*xx) ) \
              * np.exp(-1/2*(xx/sigma)**2 + xx*xx_0/sigma**2 - 1/2*xx_0**2/(sigma**2))/(sigma**2)
      return ff


   def scalar_test_function_1D_derivative_d2_dx2(xx, *args):
      xx_0  = args[0]
      sigma = args[1]
      omega = args[2]
      ff    = ( 2*omega*sigma**2*xx*np.sin(omega*xx) + xx_0**2*np.cos(omega*xx) \
                - 2 * (omega*sigma**2*np.sin(omega*xx) + xx*np.cos(omega*xx))*xx_0 \
                - (omega**2*sigma**4 + sigma**2 - xx**2) * np.cos(omega*xx) ) \
              * np.exp(-1/2*xx**2/sigma**2 + xx*xx_0/sigma**2 - 1/2*(xx_0/sigma)**2)/sigma**4
      return ff


   def scalar_test_function_2D(xx, yy, *args):
      xx_0    = args[0]
      yy_0    = args[1]
      sigma_x = args[2]
      sigma_y = args[3]
      omega_x = args[4]
      omega_y = args[5]
      ff      =   np.exp(-((xx-xx_0)/(np.sqrt(2)*sigma_x))**2) * np.cos(omega_x*xx)  \
                * np.exp(-((yy-yy_0)/(np.sqrt(2)*sigma_y))**2) * np.sin(omega_y*yy)
      return ff


   def scalar_test_function_2D_derivative_d_dx(xx, yy, *args):
      xx_0    = args[0]
      yy_0    = args[1]
      sigma_x = args[2]
      sigma_y = args[3]
      omega_x = args[4]
      omega_y = args[5]
      ff      = - ( omega_x*sigma_x**2*np.sin(omega_x*xx) + xx*np.cos(omega_x*xx) - xx_0*np.cos(omega_x*xx) ) \
                * np.exp( -1/2*xx**2/sigma_x**2 + xx*xx_0/sigma_x**2 - 1/2*xx_0**2/sigma_x**2 \
                          -1/2*yy**2/sigma_y**2 + yy*yy_0/sigma_y**2 - 1/2*yy_0**2/sigma_y**2 ) \
                * np.sin(omega_y*yy)/sigma_x**2
      return ff


   def scalar_test_function_2D_derivative_d_dy(xx, yy, *args):
      xx_0    = args[0]
      yy_0    = args[1]
      sigma_x = args[2]
      sigma_y = args[3]
      omega_x = args[4]
      omega_y = args[5]
      ff      = ( omega_y*sigma_y**2*np.cos(omega_x*xx)*np.cos(omega_y*yy)*np.exp(xx*xx_0/sigma_x**2) \
                  - yy*np.cos(omega_x*xx)*np.exp(xx*xx_0/sigma_x**2)*np.sin(omega_y*yy) \
                  + xx_0*np.cos(omega_x*xx)*np.exp(xx*xx_0/sigma_x**2)*np.sin(omega_y*yy) ) \
                * np.exp(-1/2*xx**2/sigma_x**2 - 1/2*xx_0**2/sigma_x**2 - 1/2*yy**2/sigma_y**2 + yy*yy_0/sigma_y**2 - 1/2*yy_0**2/sigma_y**2)/sigma_y**2
      return ff


   def scalar_test_function_2D_derivative_d2_dy_dx(xx, yy, *args):
      xx_0    = args[0]
      yy_0    = args[1]
      sigma_x = args[2]
      sigma_y = args[3]
      omega_x = args[4]
      omega_y = args[5]
      ff      =  ( ( omega_x*sigma_x**2*np.sin(omega_x*xx) + xx*np.cos(omega_x*xx) - xx_0*np.cos(omega_x*xx) ) \
                   * yy*np.exp(xx*xx_0/sigma_x**2)*np.sin(omega_y*yy) \
                   - ( omega_x*sigma_x**2*np.sin(omega_x*xx) + xx*np.cos(omega_x*xx) - xx_0*np.cos(omega_x*xx) ) \
                   * yy_0*np.exp(xx*xx_0/sigma_x**2)*np.sin(omega_y*yy) \
                   - (   omega_x*omega_y*sigma_x**2*sigma_y**2*np.sin(omega_x*xx) \
                      + omega_y*sigma_y**2*xx*np.cos(omega_x*xx) \
                      - omega_y*sigma_y**2*xx_0*np.cos(omega_x*xx) ) \
                   * np.cos(omega_y*yy)*np.exp(xx*xx_0/sigma_x**2) )  \
                 * np.exp(-1/2*xx**2/sigma_x**2 - 1/2*xx_0**2/sigma_x**2 \
                     - 1/2*yy**2/sigma_y**2 + yy*yy_0/sigma_y**2 - 1/2*yy_0**2/sigma_y**2)/(sigma_x**2*sigma_y**2)
      return ff



   xx = np.linspace(0, 10, 1000)
   yy = np.linspace(0,  5,  50)
   zz = np.linspace(0, 20, 400)


   if derivatives_1D:

      rr = [xx]
      XX = np.meshgrid(xx)

      ff = scalar_test_function_1D(xx, 5.0, 1.5, 3.0)

      dff_dx     = derivative(xx, [[1, 1]], 4, scalar_test_function_1D, 5.0, 1.5, 3.0)
      dff_dx_ref = scalar_test_function_1D_derivative_d_dx(xx, 5.0, 1.5, 3.0)

      d2ff_dx2     = derivative(xx, [[1, 2]], 4, scalar_test_function_1D, 5.0, 1.5, 3.0)
      d2ff_dx2_ref = scalar_test_function_1D_derivative_d2_dx2(xx, 5.0, 1.5, 3.0)


      plt.figure(1,figsize=(20,5))

      ax = []

      ax.append(plt.subplot(1,2,1))
      plo.plot_line(rr[0], ff, color='b')
      plo.plot_line(rr[0], dff_dx, color='r')
      plo.plot_line(rr[0], dff_dx_ref, color='r', linestyle=':')
      plo.plot_line(rr[0], d2ff_dx2, color='y')
      plo.plot_line(rr[0], d2ff_dx2_ref, color='y', linestyle=':')

      ax.append(plt.subplot(1,2,2))
      plo.plot_line(rr[0], dff_dx_ref-dff_dx, color='r', linestyle=':')
      plo.plot_line(rr[0], d2ff_dx2_ref-d2ff_dx2, color='y', linestyle=':')


   if derivatives_2D:

      rr = [xx, yy]
      XX, YY = np.meshgrid(xx, yy)

      ff = scalar_test_function_2D(XX, YY, 5.0, 2.5, 1.5, 1.2, 3.0, 2.0)

      dff_dx     = derivative(rr, [[1, 1]], 4, scalar_test_function_2D, 5.0, 2.5, 1.5, 1.2, 3.0, 2.0)
      dff_dx_ref = scalar_test_function_2D_derivative_d_dx(XX, YY, 5.0, 2.5, 1.5, 1.2, 3.0, 2.0)

      dff_dy     = derivative(rr, [[2, 1]], 4, scalar_test_function_2D, 5.0, 2.5, 1.5, 1.2, 3.0, 2.0)
      dff_dy_ref = scalar_test_function_2D_derivative_d_dy(XX, YY, 5.0, 2.5, 1.5, 1.2, 3.0, 2.0)


      d2ff_dx_dy     = derivative(rr, [[1, 1],[2, 1]], 4, scalar_test_function_2D, 5.0, 2.5, 1.5, 1.2, 3.0, 2.0)
      d2ff_dx_dy_ref = scalar_test_function_2D_derivative_d2_dy_dx(XX, YY, 5.0, 2.5, 1.5, 1.2, 3.0, 2.0)

      #d2ff_dy2_bd = derivative_bd(rr, [[1, 1],[1, 1]], 4, scalar_test_function_2D, [5,2.5,0], [1.5,1.2,0], [3.0,2.0,0])


      plt.figure(2,figsize=(20,20))

      ax = []

      ax.append(plt.subplot(2,2,1))
      ax[0].set_aspect('equal')
      cplt_lines, cplt_fill = plo.plot_contour_lines_and_fill(XX, YY, ff, 20, cl={'colors':'k','linewidths':0.5}, cf={'cmap':"RdBu_r"})
      plt.colorbar(cplt_fill)

      ax.append(plt.subplot(2,2,2))
      ax[1].set_aspect('equal')
      cplt_lines, cplt_fill = plo.plot_contour_lines_and_fill(XX, YY, dff_dx, 20, cl={'colors':'k','linewidths':0.5}, cf={'cmap':"RdBu_r"})
      plt.colorbar(cplt_fill)

      ax.append(plt.subplot(2,2,3))
      ax[2].set_aspect('equal')
      cplt_lines, cplt_fill = plo.plot_contour_lines_and_fill(XX, YY, dff_dx_ref, 20, cl={'colors':'k','linewidths':0.5}, cf={'cmap':"RdBu_r"})
      plt.colorbar(cplt_fill)

      ax.append(plt.subplot(2,2,4))
      ax[3].set_aspect('equal')
      cplt_lines, cplt_fill = plo.plot_contour_lines_and_fill(XX, YY, dff_dx_ref-dff_dx, 20, cl={'colors':'k','linewidths':0.5}, cf={'cmap':"RdBu_r"})
      plt.colorbar(cplt_fill)

      plt.subplots_adjust(top=0.98, bottom=0.05, left=0.05, right=0.95, hspace=0.15, wspace=0.10)


      plt.figure(3,figsize=(20,20))

      ax = []

      ax.append(plt.subplot(2,2,1))
      ax[0].set_aspect('equal')
      cplt_lines, cplt_fill = plo.plot_contour_lines_and_fill(XX, YY, ff, 20, cl={'colors':'k','linewidths':0.5}, cf={'cmap':"RdBu_r"})
      plt.colorbar(cplt_fill)

      ax.append(plt.subplot(2,2,2))
      ax[1].set_aspect('equal')
      cplt_lines, cplt_fill = plo.plot_contour_lines_and_fill(XX, YY, dff_dy, 20, cl={'colors':'k','linewidths':0.5}, cf={'cmap':"RdBu_r"})
      plt.colorbar(cplt_fill)

      ax.append(plt.subplot(2,2,3))
      ax[2].set_aspect('equal')
      cplt_lines, cplt_fill = plo.plot_contour_lines_and_fill(XX, YY, dff_dy_ref, 20, cl={'colors':'k','linewidths':0.5}, cf={'cmap':"RdBu_r"})
      plt.colorbar(cplt_fill)

      ax.append(plt.subplot(2,2,4))
      ax[3].set_aspect('equal')
      cplt_lines, cplt_fill = plo.plot_contour_lines_and_fill(XX, YY, dff_dy_ref-dff_dy, 20, cl={'colors':'k','linewidths':0.5}, cf={'cmap':"RdBu_r"})
      plt.colorbar(cplt_fill)

      plt.subplots_adjust(top=0.98, bottom=0.05, left=0.05, right=0.95, hspace=0.15, wspace=0.10)


      plt.figure(4,figsize=(20,20))

      ax = []

      ax.append(plt.subplot(2,2,1))
      ax[0].set_aspect('equal')
      cplt_lines, cplt_fill = plo.plot_contour_lines_and_fill(XX, YY, ff, 20, cl={'colors':'k','linewidths':0.5}, cf={'cmap':"RdBu_r"})
      plt.colorbar(cplt_fill)

      ax.append(plt.subplot(2,2,2))
      ax[1].set_aspect('equal')
      cplt_lines, cplt_fill = plo.plot_contour_lines_and_fill(XX, YY, d2ff_dx_dy, 20, cl={'colors':'k','linewidths':0.5}, cf={'cmap':"RdBu_r"})
      plt.colorbar(cplt_fill)

      ax.append(plt.subplot(2,2,3))
      ax[2].set_aspect('equal')
      cplt_lines, cplt_fill = plo.plot_contour_lines_and_fill(XX, YY, d2ff_dx_dy_ref, 20, cl={'colors':'k','linewidths':0.5}, cf={'cmap':"RdBu_r"})
      plt.colorbar(cplt_fill)

      ax.append(plt.subplot(2,2,4))
      ax[3].set_aspect('equal')
      cplt_lines, cplt_fill = plo.plot_contour_lines_and_fill(XX, YY, d2ff_dx_dy_ref-d2ff_dx_dy, 20, cl={'colors':'k','linewidths':0.5}, cf={'cmap':"RdBu_r"})
      plt.colorbar(cplt_fill)

      plt.subplots_adjust(top=0.98, bottom=0.05, left=0.05, right=0.95, hspace=0.15, wspace=0.10)





plt.show()

